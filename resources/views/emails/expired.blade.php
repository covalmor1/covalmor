<p>Rekap Credit Approval Expired dalam 2 Bulan Terakhir</p>
<hr>
<table>	
	<tr><th>Customer</th><th>Perkiraan Nilai Transaksi</th><th>Jangka Waktu Akhir</th></tr>
	@foreach ($data as $key)
	<tr><td>{{ $key->Customer->name }}</td><td>Rp. {{ number_format($key->nilai_transaksi) }}</td><td>{{ date('d/m/Y', strtotime($key->tempo_end)) }}</td></tr>
	@endforeach
</table>
<hr>