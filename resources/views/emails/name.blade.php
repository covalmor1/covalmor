<p>Credit Approval dengan nomor {{$data->no_tiket}} </p>
<hr>
<table>
		<tr><td>Kode</td><td>: {{ $data->Customer->customer_no }}</td></tr>
		<tr><td>Nama</td><td>: {{ $data->Customer->name }}</td></tr>
		<tr><td>NPWP</td><td>: {{ $data->Customer->npwp }}</td></tr>
		<tr><td>Alamat</td><td>: {{ $data->Customer->address }}</td></tr>
</table>
<hr>
<p>Status Credit saat ini <strong>{{ $data->Status->name }}</strong> </p>
<p>Silahkan login dan proses pada <a href="https://covalmor1.com/public/Kredit/{{ $data->id }}">Credit Approval Online</a> </p>
