<style type="text/css">
	.fall{font-size:14px;}
</style>
<div style="text-align:right!important; margin-top: -20px;" ><img src="{{ asset('beagle/img/pertamina.png') }}" alt="logo"></div>
<h2 style="text-align:center">CREDIT APPROVAL</h2>
 	@if ($data['kredit']->jaminan!='Tidak Ada')
		<h4 style="text-align:center; margin-top:-20px;">DENGAN JAMINAN</h4>
	@endif
<p style="font-weight:bold; margin-top:-20px;text-align:center;">Nomor : <?php echo str_replace(" ", "&nbsp;", $data['kredit']->no_surat); ?> Tgl. {{date('d F Y', strtotime($data['kredit']->created_at))}}</p>
<p class="fall">Dalam rangka mengantisipasi peningkatan penjualan serta memberikan salah satu fasilitas kepada pelanggan yang dilayani oleh Mitra Bisnis Transaksi BBM Pertamina {{ $data['kredit']->Customer->name }}, ditetapkan strategi pemasaran yang salah satu implementasinya adalah dengan fasilitas Penjualan Non Tunai.</p>

<p class="fall">Berdasarkan evaluasi dan pertimbangan aspek bisnis, PT Pertamina (Persero) dengan ini memberikan persetujuan kepada :</p> 
<table class="fall">
	<tr>
		<td width="200px">Nama Perusahaan</td>
		<td>: {{ $data['kredit']->Customer->name }}</td>
	</tr>
	<tr>
		<td>Customer ID</td>
		<td>: {{ $data['kredit']->Customer->customer_no }}</td>
	</tr>
	<tr>
		<td>Alamat</td>
		<td>: {{ $data['kredit']->Customer->address }}</td>
	</tr>
</table>

<p class="fall">Untuk diberikan Fasilitas Penjualan Non Tunai 
@if ($data['kredit']->jaminan!='Tidak Ada')
Dengan Jaminan
@else
Tanpa Jaminan
@endif
atas pembelian produk dari PT Pertamina (Persero) dengan ketentuan sebagai berikut : </p>

<table class="fall">
	<tr>
		<td width="240px">1. Jangka Waktu Credit Approval</td>
		<td>: {{date('d F Y', strtotime($data['kredit']->tempo_start))}} s.d {{date('d F Y', strtotime($data['kredit']->tempo_end))}}</td>
	</tr>
	<tr>
		<td width="240px">2. Jenis Produk *)</td>
		<td>: 
			@foreach ($data['kredit']->Product as $no => $value)
	            @if ($no!=0)
	            , 
	            @endif
	              {{$value->Detail->material_name}}
	          @endforeach
	     </td>
	</tr>
	<tr>
		<td width="240px">3. Perkiraan Volume/Quantity</td>
		<td>:  {{ number_format ( $data['kredit']->volume , 0 , "." , ".")}} {{ $data['kredit']->satuan }} per {{ $data['kredit']->periode_volume }} 
		 	<!-- @if (substr($data['kredit']->periode_penyerahan, -2)=='an' &&  $data['kredit']->periode_penyerahan!='Triwulan')
				{{substr($data['kredit']->periode_penyerahan, 0, -2)}}
			@else
				{{$data['kredit']->periode_penyerahan}}
			@endif -->
		</td>
	</tr>
	<tr>
		<td width="240px">4. Perkiraan Nilai Transaksi</td>
		<td>: Rp. {{ number_format ( $data['kredit']->nilai_transaksi , 0 , "." , ".")}},- /  {{ $data['kredit']->periode_volume }} 
		 	<!-- @if (substr($data['kredit']->periode_penyerahan, -2)=='an' &&  $data['kredit']->periode_penyerahan!='Triwulan')
				{{substr($data['kredit']->periode_penyerahan, 0, -2)}}
			@else
				{{$data['kredit']->periode_penyerahan}}
			@endif -->
		</td>
	</tr>
	<tr>
		<td width="240px">5. Credit Limit</td>
		<td>: Rp. {{ number_format ( $data['kredit']->credit_limit , 0 , "." , ".")}},-</td>
	</tr>
	<tr>
		<td width="240px">6. Term of Payment : </td>
	</tr>
</table>

<table class="fall" style="text-align: center!important; border-collapse: collapse;margin: 0 auto;">
	<tr>
		<td style="border: 1px solid black; padding: 0 20px;">Periode Penyerahan BBM</td>
		<td style="border: 1px solid black;">Jatuh Tempo Pembayaran</td>
	</tr>
	<tr>
		<td style="border: 1px solid black;">{{ $data['kredit']->periode_penyerahan }}</td>
		<td style="border: 1px solid black; padding: 0 20px;">{{ $data['kredit']->lama_tempo }} {{ $data['dayday'] }} Setelah Penyerahan BBM</td>
	</tr>
</table>

<table class="fall">
	<tr>
		<td width="240px">7. Mekanisme Pembayaran</td>
		<td>: Melalui {{ $data['kredit']->pembayaran }}</td>
	</tr>
	<tr>
		<td width="240px">8. Bentuk Jaminan yang diusulkan</td>
		<td>: {{ $data['kredit']->jaminan }}</td>
	</tr>
	<tr>
		<td width="240px">9. Sanksi Keterlambatan</td>
		<td>:</td>
		<td rowspan="2" style="border:1px solid black; height: 30px; margin-left: -128px!important; padding-left: 10px;"><b>[</b><i style="font-style: italic;border-top:0.1px solid black">Rn</i> <b>+ 4%] x nilai tunggakan x n/360</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
	</tr>
</table>
<table class="fall">
	<tr>
		<td width="248px">&nbsp;</td>
		<td>
				<u>Keterangan : </u>
			<br><i style="font-style: italic;border-top:0.1px solid black;height: 30px;">Rn</i> : rata-rata JIBOR rate 1 bulan pada hari ke-n
			<br>n : hari keterlambatan
		</td>
	</tr>
</table>
<table class="fall">
	<tr>
		<td width="240px">10. Syarat Penyerahan</td>
		<td>: {{ $data['kredit']->syarat_penyerahan }}</td>
	</tr>
	<tr>
		<td colspan="4">11. Apabila tidak melakukan pembayaran setelah jatuh tempo pembayaran, maka penyerahan BBM akan dihentikan terhitung 1 hari kalender sejak tanggal jatuh tempo pembayaran.</td>
	</tr>
</table>

<br>
<table>
	<tr>
		<td><b>PT PERTAMINA (PERSERO)</b></td>
		<td style="width:200px">&nbsp;</td>
		<td></td>
	</tr>
	<tr>
		<td>Marketing Operation Region I</td>
		<td style="width:200px">&nbsp;</td>
		<td>Finance Marketing Operation Region I</td>
	</tr>
	<tr>
		<td>{{ $data['ttd']->position1 }}</td>
		<td style="width:200px">&nbsp;</td>
		<td>{{ $data['ttd']->position2 }}</td>
	</tr>

	<tr><td>&nbsp;</td></tr>
	<tr><td>&nbsp;</td></tr>
	<tr><td>&nbsp;</td></tr>
	<tr><td>&nbsp;</td></tr>

	<tr>
		<td><b>{{ $data['ttd']->name1 }}</b></td>
		<td style="width:200px">&nbsp;</td>
		<td><b>{{ $data['ttd']->name2 }}</b></td>
	</tr>

</table>
	  

