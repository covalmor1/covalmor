<style type="text/css">
	.fall{font-size:14px;}
</style>
<div style="text-align:right!important" ><img src="{{ asset('beagle/img/pertamina.png') }}" alt="logo"></div>
<h1>NOTA</h1>
<p>Medan, {{date('d F Y', strtotime($data['nota']->tanggal_nota))}}</p>

<table class="fall">
	<tr>
		<td width="100px">Kepada</td>
		<td> : Manager Finance MOR I</td>
	</tr>
	<tr>
		<td>Dari</td>
		<td>: Ast. Man. Finance Business Support Region I</td>
	</tr>
	<tr>
		<td>Perihal</td>
		<td>: {{ $data['nota']->perihal }}</td>
	</tr>
</table>

<p class="fall">{{ $data['nota']->isi }}</p>
<p>Demikian disampaikan, atas perhatian dan kerja samanya kami ucapkan terima kasih.</p>
<p>{{ $data['nota']->Creator->jabatan }}</p>
<br><br><br><br>
<p><b>{{ $data['nota']->Creator->name }}</b></p>
