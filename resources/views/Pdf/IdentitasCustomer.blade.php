<style type="text/css">
	.fall{font-size:14px;}
</style>
<div style="text-align:right!important; margin-top: -20px;" ><img src="{{ asset('beagle/img/pertamina.png') }}" alt="logo"></div>
<h2 style="text-align:center">IDENTITAS CUSTOMER</h2>

<table>	
	<tr>
		<td></td>
	</tr>

	<tr>
		<td>Jenis Industri</td>	<td>: {{ $data->jenis_industri }}</td>
	</tr>	
	<tr>
		<td>Rata-Rata Keterlambatan Customer 1 Tahun Terakhir</td>	<td>: {{ $data->keterlambatan }}</td>
	</tr>	
	<tr>
		<td>Riwayat Restrukturisasi</td>	<td>: {{ $data->restrukturisasi }}</td>
	</tr>	
	<tr>
		<td>Fasilitas Kredit Oleh Perbankan</td>	<td>: {{ $data->fasilitas_kredit }}</td>
	</tr>	
	<tr>
		<td>Lama Bekerjasama dengan Pertamina</td>	<td>: {{ $data->lama_kerjasama }}</td>
	</tr>	
	<tr>
		<td>Pelanggan Sebagai Vendor/Pemasok Pertamina</td>	<td>: {{ $data->vendor_pemasok }} </td>
	</tr>	
	<tr>
		<td>Posisi Tawar Pertamina terhadap Customer</td>	<td>: {{ $data->posisi_tawar }}	</td>
	</tr>
	<tr>
		<td>Badan Usaha</td>	<td>: {{ $data->badan_usaha }}</td>
	</tr>	
	<tr>
		<td>Affiliasi</td>	<td>: {{ $data->afiliasi }}</td>
	</tr>	
	<tr>
		<td>Kondisi Industri</td>	<td>: {{ $data->kondisi_industri }}	</td>
	</tr>
	<tr>
		<td>Opini Audit</td>	<td>: {{ $data->opini_audit }}</td>
	</tr>	
	<tr>
		<td>Diaudit oleh KAP terdaftar di OJK</td>	<td>: {{ $data->audit_kap }}</td>
	</tr>
</table>	  

