@extends('layouts.app')
@section('content')

      @if ($errors->any())
      <div class="alert alert-danger">
          <ul>
              @foreach ($errors->all() as $error)
                  <li>{{ $error }}</li>
              @endforeach
          </ul>
      </div><br />
      @endif
      @if (\Session::has('success'))
      <div class="alert alert-success">
          <p>{{ \Session::get('success') }}</p>
      </div><br />
      @endif

      <div class="panel panel-default panel-border-color panel-border-color-primary">
                <div class="panel-body">

      <div class="row m-t-sm">
                                <div class="col-lg-12">
                                <div class="panel blank-panel">
                                <div class="panel-heading">
                                    <div class="panel-options">
                                        <ul class="nav nav-tabs">
                                            <li class="active"><a href="#tab-1" data-toggle="tab">Edit</a></li>
                                        </ul>
                                    </div>
                                </div>

                                <div class="panel-body">

                                <div class="tab-content">
                                <div class="tab-pane active" id="tab-1">

      <form method="post" action="{{action('Master\CustomerController@update', $customer->id )}}">
        {{csrf_field()}}
        
        <div class="row">
          <div class="col-lg-12 margin-tb"></div>
          <div class="form-group col-md-8">
            <label for="nama_penyulang">No. Customer:</label>
            <input type="text" name="customer_no" class="form-control" value="{{ $customer->customer_no }}">
          </div>
        </div>
        
        <div class="row">
          <div class="col-lg-12 margin-tb"></div>
          <div class="form-group col-md-8">
            <label for="nama_penyulang">Nama:</label>
          <input type="text" name="name" class="form-control" value="{{ $customer->name }}">
          </div>
        </div>

        <div class="row">
          <div class="col-lg-12 margin-tb"></div>
          <div class="form-group col-md-8">
            <label for="nama_penyulang">Email:</label>
            <input type="text" name="email" class="form-control" value="{{ $customer->email }}">
          </div>
        </div>
        
        <div class="row">
          <div class="col-lg-12 margin-tb"></div>
          <div class="form-group col-md-8">
            <label for="nama_penyulang">Alamat:</label>
            <input type="text" name="address" class="form-control" value="{{ $customer->address }}">
          </div>
        </div>

        <div class="row">
          <div class="col-lg-12 margin-tb"></div>
          <div class="form-group col-md-8">
            <label for="nama_penyulang">NPWP:</label>
            <input type="text" name="npwp" class="form-control" value="{{ $customer->npwp }}">
          </div>
        </div>

        <div class="row">
           <div class="col-lg-12 margin-tb"></div>
          <div class="form-group col-md-4">
            <button type="submit" class="btn btn-primary btn-sm" style="margin-left:18px">Submit</button>
          </div>
        </div>
      </form>

      </div>
        </div>
        </div>
        </div>
          </div>
        </div>
      </div>
@endsection  
