@extends('layouts.app')
@section('content')


    @if (\Session::has('success'))
      <div class="alert alert-success">
        <p>{{ \Session::get('success') }}</p>
      </div><br />
     @endif

<div class="row">
 <div class="col-lg-12 margin-tb"></div>
 <div class="form-group col-md-1">
<div class="nav navbar-right">
   <a href="{{ url ('/Master/Product/create') }}" class="btn btn-success btn-sm">+ Tambah</a>
</div>

</div>

<div class="col-lg-6">
  <!-- <a onclick="myFunction()" id="href" class="btn btn-success btn-sm" type="submit"><i class="fa fa-download"></i>Export</a> -->
  <a href="#upload-update" class="btn btn-success btn-sm" data-toggle="modal" data-target="#upload-modal-update"><i class="fa fa-upload"></i> Upload Product</a> 
</div>

<!-- <div class="col">
</div>
 -->
<!-- <div class="col-md-1 pull-right">
    <a data-toggle="modal" class="btn btn-sm btn-primary" href="#modal-form">Filter</a>
</div> -->
</div>

<div class="panel panel-default panel-border-color panel-border-color-danger">
                <div class="panel-heading panel-heading-divider">Master Product</div>
                <div class="panel-body">
    <div class="table-responsive">
    <table id="table3" class="table table-striped">
    <thead>
      <tr>

        <th>No. Product</th>
        <th>Nama Product</th>
        <th></th>
        <th></th>
      </tr>
    </thead>
     <tbody>
      @foreach ($product as $key)
      <tr>
        <td>{{ $key->material_no }}</td>
        <td>{{ $key->material_name}}</td>
        <td>   <a href="{{action('Master\ProductController@edit', $key->id )}}" title="Edit"><span class="icon mdi mdi-edit"></span></a>
          </td>
          <td>
            <form action="{{action('Master\ProductController@destroy', $key->id )}}" method="post">
            {{csrf_field()}}
            <input name="_method" type="hidden" value="DELETE">
            <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
            <button class="icon mdi mdi-delete" title="Delete" onclick="return confirm('Apakah Anda Yakin Ingin Hapus data ini?');" class="icon"></button>
          </form>
        </td>
      </tr>
      @endforeach
      
    </tbody>
  
  </table>
</div>
</div>
</div>

<div id="modal-form" class="modal fade" aria-hidden="true">
                                      <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-body">
                                                <div class="row">
                                                    <div class="col-sm-2">
                                                    <h3 class="m-t-none m-b">Filter By:</h3>
                                                    </div>
                                                    <div class="col-sm-10">

                                                        <form role="form" method="post" action="">
                                                          {{csrf_field()}}
                                                            <div class="form-group" id="data_1">
                                                              <label class="font-normal">Dari Tanggal</label>
                                                                    <div class="input-group date">
                                                                      <span class="input-group-addon"><i class="fa fa-calendar"></i></span><input type="text" class="form-control" name="from_tanggal" value="{{Carbon\Carbon::today()->format('Y-m-d')}}">
                                                                     </div>
                                                                </div>
                                                                <div class="form-group" id="data_1">
                                                              <label class="font-normal">Ke Tanggal</label>
                                                                    <div class="input-group date">
                                                                      <span class="input-group-addon"><i class="fa fa-calendar"></i></span><input type="text" class="form-control" name="to_tanggal" value="{{Carbon\Carbon::today()->format('Y-m-d')}}">
                                                                     </div>
                                                                </div>
                                                            <div>
                                                                <input class="btn btn-sm btn-primary pull-right m-t-n-xs" type="submit" name="filter" value="Filter">
                                                                <label> 
                                                            </div>
                                                        </form>
                                                </div>
                                            </div>
                                        </div>
                                        </div>
                                    </div>
                            </div>





<!-- Upload Modal Update -->
<div id="upload-modal-update" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Upload Product</h4>
    </div>
    <div class="modal-body">
        <form id="form-upload2" class="form-horizontal" method="post" action="{{ url('Master/Product/UploadUpdate') }}" enctype="multipart/form-data">
            {{csrf_field()}}
            <div class="box-body text-center" >
                <div id="file_name2"></div>
                <label class="btn btn-default btn-lg" style="position: relative; overflow:hidden">
                    Browse <input name="file" type="file" style="display: none" onChange="$('#file_name2').html(this.files[0].name)">
                </label>
            </div>
        </form>
    </div>
    <div class="modal-footer">
        <a href="{{ asset('beagle/product_template.xlsx') }}" class="btn btn-warning" ><i class="fa fa-download"></i> Download Template</a>
        <button type="button" class="btn btn-success" data-dismiss="modal" onclick="$('#form-upload2').submit()"><i class="fa fa-upload"></i> Upload</button>
    </div>
</div>
s
</div>
</div><!-- Upload modal -->

  <script>
   // $(".delete").on("submit", function(){
    //    return confirm("Do you want to delete this item?");
    //});
</script>

<script>
function myFunction() {
    var search = "Product/Export"+window.location.search;
    document.getElementById("href").href = search;
}

</script>
 @endsection   