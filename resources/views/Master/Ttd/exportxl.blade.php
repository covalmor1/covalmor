<!-- <table>
    <thead>
        <tr>
            <th>SP</th>
            <th>NAMA</th>
            <th>EMAIL</th>
            <th>ALAMAT</th>
            <th>NPWP</th>
        </tr>
    </thead>
    @foreach($customer as $data)
        <tr>
            <td>{{ $data->customer_no}}</td>
            <td>{{ $data->name}}</td>
            <td>{{ $data->email}}</td>
            <td>{{ $data->address}}</td>
            <td>{{ $data->npwp}}</td>
        </tr>
    @endforeach
</table> -->


@extends('layouts.app')
@section('content')


    @if (\Session::has('success'))
      <div class="alert alert-success">
        <p>{{ \Session::get('success') }}</p>
      </div><br />
     @endif

<div class="row">
 <div class="col-lg-12 margin-tb"></div>
 <div class="form-group col-md-1">
<div class="nav navbar-right">
   <a href="{{ url ('/Master/Customer/create') }}" class="btn btn-success btn-sm">Add</a>
</div>
</div>

<div class="col-md-2">
    <a onclick="myFunction()" id="href" class="btn btn-success btn-sm" type="submit"><i class="fa fa-download"></i>Export</a>
</div>


<div class="col-md-1 pull-right">
    <a data-toggle="modal" class="btn btn-sm btn-primary" href="#modal-form">Filter</a>
</div>
</div>

<div class="panel panel-default panel-border-color panel-border-color-primary">
                <div class="panel-heading panel-heading-divider">Master Customer</div>
                <div class="panel-body">
    <div class="table-responsive">
    <table class="table table-striped">
    <thead>
      <tr>

        <th>SP</th>
        <th>Nama</th>
        <th>Email</th>
        <th>Alamat</th>
        <th>NPWP</th>
        <th></th>
        <th></th>
      </tr>
    </thead>
     <tbody>
    @foreach($customer as $data)
        <tr>
            <td>{{ $data->customer_no}}</td>
            <td>{{ $data->name}}</td>
            <td>{{ $data->email}}</td>
            <td>{{ $data->address}}</td>
            <td>{{ $data->npwp}}</td>
        </tr>
    @endforeach
      
    </tbody>
  
  </table>
</div>
</div>
</div>


