@extends('layouts.app')
@section('content')

      @if ($errors->any())
      <div class="alert alert-danger">
          <ul>
              @foreach ($errors->all() as $error)
                  <li>{{ $error }}</li>
              @endforeach
          </ul>
      </div><br />
      @endif
      @if (\Session::has('success'))
      <div class="alert alert-success">
          <p>{{ \Session::get('success') }}</p>
      </div><br />
      @endif

      <div class="panel panel-default panel-border-color panel-border-color-primary">
                <div class="panel-body">

      <div class="row m-t-sm">
                                <div class="col-lg-12">
                                <div class="panel blank-panel">
                                <div class="panel-heading">
                                    <div class="panel-options">
                                        <ul class="nav nav-tabs">
                                            <li class="active"><a href="#tab-1" data-toggle="tab">Edit</a></li>
                                        </ul>
                                    </div>
                                </div>

                                <div class="panel-body">

                                <div class="tab-content">
                                <div class="tab-pane active" id="tab-1">

      <form method="post" action="{{action('Master\TtdController@update', $ttd->id )}}">
        {{csrf_field()}}

        <div class="row">
          <div class="col-lg-12 margin-tb"></div>
          <div class="form-group col-md-8">
            <label for="nama_penyulang">Nama 1 :</label>
            <input type="text" name="name1" class="form-control" value="{{ $ttd->name1 }}">
          </div>
        </div>

        <div class="row">
          <div class="col-lg-12 margin-tb"></div>
          <div class="form-group col-md-8">
            <label for="nama_penyulang">Jabatan 1 :</label>
            <input type="text" name="position1" class="form-control" value="{{ $ttd->position1 }}">
          </div>
        </div>

        <div class="row">
          <div class="col-lg-12 margin-tb"></div>
          <div class="form-group col-md-8">
            <label for="nama_penyulang">Nama 2 :</label>
            <input type="text" name="name2" class="form-control" value="{{ $ttd->name2 }}">
          </div>
        </div>

        <div class="row">
          <div class="col-lg-12 margin-tb"></div>
          <div class="form-group col-md-8">
            <label for="nama_penyulang">Jabatan 2 :</label>
            <input type="text" name="position2" class="form-control" value="{{ $ttd->position2 }}">
          </div>
        </div>
        
        <div class="row">
          <div class="col-lg-12 margin-tb"></div>
          <div class="form-group col-md-8">
            <label for="nama_penyulang">Jenis Dokumen:</label>
             <select type="text" name="document_type" class="select2_demo_2 form-control">
                <option value="CA" {{ ($ttd->document_type=='CA' ? 'selected':'')}}>Credit Approval</option>
                <!-- <option value="NOTA" {{ ($ttd->document_type=='NOTA' ? 'selected':'')}}>NOTA</option> -->
              </select>     
          </div>
          </div>
        </div>

        <div class="row">
           <div class="col-lg-12 margin-tb"></div>
          <div class="form-group col-md-4">
            <button type="submit" class="btn btn-primary btn-sm" style="margin-left:18px">Submit</button>
          </div>
        </div>
      </form>

      </div>
        </div>
        </div>
        </div>
          </div>
        </div>
      </div>
@endsection  
