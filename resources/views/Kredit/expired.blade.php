@extends('layouts.app')
@section('content')


    @if (\Session::has('success'))
      <div class="alert alert-success">
        <p>{{ \Session::get('success') }}</p>
      </div><br />
     @endif

<div class="panel panel-default panel-border-color panel-border-color-danger">
                <div class="panel-heading panel-heading-divider">Credit Approval Expired List</div>
                <div class="panel-body">
    <div class="table-responsive">
    <table id="table3" class="table table-striped">
    <thead>
      <tr>
        <th data-visible="false">No Tiket</th>
        <th>Customer</th>
        <th>Jenis Produk</th>
        <th>Perkiraan Volume</th>
        <th data-visible="false">UOM</th>
        <th>Perkiraan Nilai Transaksi</th>
        <th data-visible="false">Mekanisme Pembayaran</th>
        <th data-visible="false">Bentuk Jaminan</th>
        <th data-visible="false">Sanksi Keterlambatan</th>
        <th data-visible="false">Syarat Penyerahan</th>
        <th data-visible="false">Jangka Waktu Mulai</th>
        <th data-visible="false">Jangka Waktu Berakhir</th>
        <th>Credit Limit</th>
        <th>Creator</th>
        <th>Status</th> 
        <th data-visible="false">Tanggal Create User</th>
        <th data-visible="false">Tanggal Submit AR</th>
        <th data-visible="false">Tanggal Submit Cashbank</th>
        <th data-visible="false">Tanggal Submit FBS</th>
        <th data-visible="false">Tanggal Submit Manajemen Resiko</th>
        <th data-visible="false">Tanggal Submit Komite Kredit</th>
        <th></th>
      </tr>
    </thead>
     <tbody>
      @foreach ($kredit as $key)
      <tr>
        <td data-visible="false">{{ $key->no_tiket }}</td>
        <td>{{ $key->Customer->customer_no }} {{ $key->Customer->name }}</td>
        <td>
          @foreach ($key->Product as $no => $value)
            @if ($no!=0)
              , 
            @endif
              {{$value->Detail->material_name}}
          @endforeach
        <td>{{ number_format($key->volume) }}</td>
        <td data-visible="false">{{ $key->satuan }}</td>
        <td>Rp. {{ number_format($key->nilai_transaksi) }}</td>
        <td data-visible="false">{{ $key->pembayaran }}</td>
        <td data-visible="false">{{ $key->jaminan }}</td>
        <td data-visible="false">{{ ($key->flag_denda=='1' ? 'Ya':'Tidak') }}</td>
        <td data-visible="false">{{ $key->syarat_penyerahan }}</td>
        <td data-visible="false">{{ $key->tempo_start }}</td>
        <td data-visible="false">{{ $key->tempo_end }}</td>
        <td>Rp. {{ number_format($key->credit_limit) }}</td>
        <td>{{ $key->Creator->name }}</td>
        <td><a style="pointer-events: none;cursor: default;" class="btn btn-sm btn-{{ $key->Status->class }}"> {{ $key->Status->name }}</a></td>
        <td data-visible="false">{{ $key->created_at }}</td>
        <td data-visible="false">{{ $key->submit_ar_date }}</td>
        <td data-visible="false">{{ $key->submit_cb_date }}</td>
        <td data-visible="false">{{ $key->submit_fbs_date }}</td>
        <td data-visible="false">{{ $key->submit_mr_date }}</td>
        <td data-visible="false">{{ $key->submit_kk_date }}</td>
        <td><a href="{{action('Kredit\KreditController@show', $key->id )}}" title="View"><span class="btn btn-sm btn-primary"><i class="mdi mdi-eye"></i></span></a></td>
      </tr>
      @endforeach
      
    </tbody>
  
  </table>
</div>
</div>
</div>
 @endsection   