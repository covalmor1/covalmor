@extends('layouts.app')

@section('afterhead')
    <link href="{{ asset('beagle/lib/select2/css/select2.min.css') }}" rel="stylesheet">
@endsection

@section('content')
      @if ($errors->any())
      <div class="alert alert-danger">
          <ul>
              @foreach ($errors->all() as $error)
                  <li>{{ $error }}</li>
              @endforeach
          </ul>
      </div><br />
      @endif
      @if (\Session::has('success'))
      <div class="alert alert-success">
          <p>{{ \Session::get('success') }}</p> 
      </div><br />
      @endif

        <div class="row">
            <div class="col-sm-12">
              <div class="panel panel-default panel-border-color panel-border-color-danger">
                <div class="panel-heading panel-heading-divider">Form Identitas Customer</div>
                <div class="panel-body">
            <form method="post" action="{{action('Kredit\CustomerDetailController@save', $kredit->id)}}" enctype="multipart/form-data">
              {{csrf_field()}}
              <div class="row">
                <div class="col-lg-12 margin-tb"></div>

                <div id="accordion1" class="panel-group accordion">
                <div class="panel panel-default">
                  <div class="panel-heading">
                    <h4 class="panel-title"><a data-toggle="collapse" data-parent="#accordion1" href="#collapseOne"><i class="icon mdi mdi-chevron-down"></i> {{ $kredit->Customer->name}}</a></h4>
                  </div>
                  <div id="collapseOne" class="panel-collapse collapse in">
                    <div class="panel-body">
                      <div class="row">
                        <div class="form-group col-sm-6">
                          <label>Kode Customer MySAP:</label>
                          <input type="text" name="customer_no" class="form-control input-sm" value="{{ $kredit->Customer->customer_no }}" readonly>
                        </div>

                        <div class="form-group col-sm-6">
                          <label>NPWP:</label>
                          <input type="text" name="customer_no" class="form-control input-sm" value="{{ $kredit->Customer->npwp }}" readonly>
                        </div>
                      </div>

                      <div class="row">
                       <div class="form-group col-sm-6">
                          <label>Alamat:</label>
                          <input type="text" name="jenis_jaminan" class="form-control input-sm" value="{{ $kredit->Customer->address }}" readonly>
                        </div>
                         
                        <div class="form-group col-sm-6">
                          <label>Bentuk Jaminan:</label>
                          <input type="text" name="jenis_jaminan" class="form-control input-sm" value="{{ $kredit->jaminan }}" readonly>
                        </div>

                      </div>
                     </div>
                  </div>
                </div>
                </div>

                <div class="form-group col-sm-6">
                  <label>Jenis Industri Customer:</label>
                  <input type="hidden" name="ca_id" value="{{ $kredit->id }}">
                  <select type="text" name="jenis_industri" class="form-control input-sm">
                      <option value="">Pilih Jenis Industri</option>
                      <option value="Industri Dasar Kimia dan Aneka Industri">Industri Dasar Kimia dan Aneka Industri</option>
                      <option value="Barang Konsumsi">Barang Konsumsi</option>
                      <option value="Jasa">Jasa</option>
                      <option value="Perdagangan">Perdagangan</option>
                      <option value="Pertambangan">Pertambangan</option>
                      <option value="Marine">Marine</option>
                      <option value="Aviasi">Aviasi</option>
                  </select>
                </div>

                <div class="form-group col-sm-6">
                  <label>Rata-Rata Keterlambatan Customer 1 Tahun Terakhir:</label>
                  <select type="text" name="keterlambatan" class="form-control input-sm">
                      <option value="">Pilih Rata Rata Keterlambatan</option>
                      <option value="30 Hari Kalendar Setelah Tanggal Jatuh Tempo">30 Hari Kalendar Setelah Tanggal Jatuh Tempo</option>
                      <option value="16 Hari Kalender < x < 30 Hari Setelah Tanggal Jatuh Tempo">16 Hari Kalender < x < 30 Hari Setelah Tanggal Jatuh Tempo</option>
                      <option value="8 Hari Kalender < x < 15 Hari Setelah Tanggal Jatuh Tempo">16 Hari Kalender < x < 30 Hari Setelah Tanggal Jatuh Tempo</option>
                      <option value="1 Hari Kalender < x < 7 Hari Setelah Tanggal Jatuh Tempo">1 Hari Kalender < x < 7 Hari Setelah Tanggal Jatuh Tempo</option>
                      <option value="Tanggal Jatuh Tempo">Tanggal Jatuh Tempo</option>
                  </select>
                </div>
              </div>

                <hr>

              <div class="row">
                <div class="col-lg-12 margin-tb"></div>
                <div class="form-group col-md-6">
                  <label>Riwayat Restrukturisasi:</label>
                  <select type="text" name="restrukturisasi" class="form-control input-sm">
                    <option value="">Pilih Riwayat Restrukturisasi</option>
                    <option value="Pernah dan Tidak Lunas">Pernah dan Tidak Lunas</option>
                    <option value="Pernah dan Berhasil (Tidak On Schedule tetapi Lunas)">Pernah dan Berhasil (Tidak On Schedule tetapi Lunas)</option>
                    <option value="Tidak Pernah">Tidak Pernah</option>
                  </select>
                </div>

                <div class="form-group col-md-6">
                  <label>Fasilitas Kredit Oleh Perbankan:</label>
                  <select type="text" name="fasilitas_kredit" class="form-control input-sm">
                    <option value="">Pilih Fasilitas Kredit</option>
                    <option value="Tidak Mempunyai">Tidak Mempunyai</option>
                    <option value="Mempunyai Fasilitas Kredit Bank Atau Tidak Mempunyai Namun Memenuhi">Mempunyai Fasilitas Kredit Bank Atau Tidak Mempunyai Namun Memenuhi</option>
                  </select>
                </div>
              </div>

              <div class="row">
                <div class="col-lg-12 margin-tb"></div>
                <div class="form-group col-md-6">
                  <label>Lama Bekerjasama dengan Pertamina:</label>
                  <select type="text" name="lama_kerjasama" class="form-control input-sm">
                    <option value="Belum Pernah">Belum Pernah</option>
                    <option value="< 1 Tahun">< 1 Tahun</option>
                    <option value="1 Tahun < x < 5 Tahun">1 Tahun < x < 5 Tahun</option>
                    <option value="x > 5 Tahun">x > 5 Tahun</option>
                  </select>
                </div>

                <div class="form-group col-md-6">
                  <label>Pelanggan Sebagai Vendor/Pemasok Pertamina:</label>
                  <select type="text" name="vendor_pemasok" class="form-control input-sm">
                    <option value="Tidak">Tidak</option>
                    <option value="Ya (Tidak Aktif)">Ya (Tidak Aktif)</option>
                    <option value="Ya (Aktif)">Ya (Aktif)</option>
                  </select>
                </div>
              </div>


                <div class="row">
                  <div class="col-lg-12 margin-tb"></div>
                  <div class="form-group col-md-6">
                    <label>Posisi Tawar Pertamina terhadap Customer:</label>
                    <select type="text" name="posisi_tawar" class="form-control input-sm">
                      <option value="Tidak Ada">Tidak Ada</option>
                      <option value="Ada">Ada</option>
                    </select>
                  </div>
               </div>

               <hr>

                <div class="row">
                  <div class="col-lg-12 margin-tb"></div>
                  <div class="form-group col-md-6">
                    <label>Badan Usaha:</label>
                    <select type="text" name="badan_usaha" class="form-control input-sm">
                      <option value="Bukan PT(Perseroan Terbatas)">Bukan PT(Perseroan Terbatas)</option>
                      <option value="PT Yang Tidak Terdaftar di BEI">PT Yang Tidak Terdaftar di BEI</option>
                      <option value="PT Yang Tidak Terdaftar di BEI Namun Multinasional">PT Yang Tidak Terdaftar di BEI Namun Multinasional</option>
                      <option value="PT yang terdaftar di BEI">PT yang terdaftar di BEI</option>
                    </select>
                  </div>

                  <div class="form-group col-md-6">
                    <label>Afiliasi:</label>
                    <select type="text" name="afiliasi" class="form-control input-sm">
                      <option value="Tidak Berafiliasi dengan Perusahaan yang Listed di BEI">Tidak Berafiliasi dengan Perusahaan yang Listed di BEI</option>
                      <option value="Tidak Berafiliasi dengan perusahaan yang Listed di BEI namun berafiliasi dengan Perusahaan Lainnya">Tidak Berafiliasi dengan perusahaan yang Listed di BEI namun berafiliasi dengan Perusahaan Lainnya</option>
                      <option value="Affiliasi Dengan PT yang terdaftar di BEI (Terbuka)">Affiliasi Dengan PT yang terdaftar di BEI (Terbuka)</option>
                    </select>
                  </div>
                </div>

              <div class="row">
              <div class="form-group col-md-6">
                  <label>Kondisi Industri:</label>
                  <select type="text" name="kondisi_industri" class="form-control input-sm">
                    <option value="Menurun">Menurun</option>
                    <option value="Stabil">Stabil</option>
                    <option value="Bertumbuh">Bertumbuh</option>
                  </select>
                </div>

                <div class="form-group col-md-6">
                    <label>Opini Audit:</label>
                    <select type="text" name="opini_audit" class="form-control input-sm">
                      <option value="Non Audited / Disclaimer / Adverse / Review">Non Audited / Disclaimer / Adverse / Review</option>
                      <option value="Audited dan Opini Wajar dengan pengecualian">Audited dan Opini Wajar dengan pengecualian</option>
                      <option value="Audited dan Opini Wajar tanpa pengecualian">Audited dan Opini Wajar tanpa pengecualian</option>
                    </select>
                  </div>
              </div>


              <div class="row">
                <div class="form-group col-md-6">
                    <label>Diaudit oleh KAP terdaftar di OJK:</label>
                    <select type="text" name="audit_kap" class="form-control input-sm">
                      <option value="">Pilih Audit KAP</option>
                      <option value="Tidak Terdaftar">Tidak Terdaftar</option>
                      <option value="Terdaftar">Terdaftar</option>
                    </select>
                  </div>
              </div>

              <div class="row">
                 <div class="col-lg-12 margin-tb"></div>
                <div class="form-group col-md-11">
                  <button type="submit" class="btn btn-primary btn-lg pull-right" style="margin-left:18px">Save</button>
                </div>
              </div>
            </form>

            </div>
          </div>
        </div>

@endsection  

@section('afterscript')
  <script src="{{ asset('beagle/lib/select2/js/select2.full.min.js') }}"></script>
  <script>
    $(".select2_demo_2").select2({
      width: '100%',
      height: '100%'
    });
  </script>
@stop
