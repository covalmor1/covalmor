@extends('layouts.app')

@section('afterhead')
    <link href="{{ asset('beagle/lib/select2/css/select2.min.css') }}" rel="stylesheet">
@endsection

@section('content')
      @if ($errors->any())
      <div class="alert alert-danger">
          <ul>
              @foreach ($errors->all() as $error)
                  <li>{{ $error }}</li>
              @endforeach
          </ul>
      </div><br />
      @endif
      @if (\Session::has('success'))
      <div class="alert alert-success">
          <p>{{ \Session::get('success') }}</p> 
      </div><br />
      @endif

        <div class="row">
            <div class="col-sm-12">
              <div class="panel panel-default panel-border-color panel-border-color-danger">
                <div class="panel-heading panel-heading-divider">Form Identitas Customer</div>
                <div class="panel-body">
            <form method="post" action="{{action('Kredit\CustomerDetailController@update', $kredit->CustomerDetail->id)}}" enctype="multipart/form-data">
              {{csrf_field()}}
              <div class="row">
                <div class="col-lg-12 margin-tb"></div>

                <div id="accordion1" class="panel-group accordion">
                <div class="panel panel-default">
                  <div class="panel-heading">
                    <h4 class="panel-title"><a data-toggle="collapse" data-parent="#accordion1" href="#collapseOne"><i class="icon mdi mdi-chevron-down"></i> {{ $kredit->Customer->name}}</a></h4>
                  </div>
                  <div id="collapseOne" class="panel-collapse collapse in">
                    <div class="panel-body">
                      <div class="row">
                        <div class="form-group col-sm-6">
                          <label>Kode Customer MySAP:</label>
                          <input type="text" name="customer_no" class="form-control input-sm" value="{{ $kredit->Customer->customer_no }}" readonly>
                        </div>

                        <div class="form-group col-sm-6">
                          <label>NPWP:</label>
                          <input type="text" name="customer_no" class="form-control input-sm" value="{{ $kredit->Customer->npwp }}" readonly>
                        </div>
                      </div>

                      <div class="row">
                       <div class="form-group col-sm-6">
                          <label>Alamat:</label>
                          <input type="text" name="jenis_jaminan" class="form-control input-sm" value="{{ $kredit->Customer->address }}" readonly>
                        </div>
                         
                        <div class="form-group col-sm-6">
                          <label>Bentuk Jaminan:</label>
                          <input type="text" name="jenis_jaminan" class="form-control input-sm" value="{{ $kredit->jaminan }}" readonly>
                        </div>

                      </div>
                     </div>
                  </div>
                </div>
                </div>

                <div class="form-group col-sm-6">
                  <label>Jenis Industri Customer:</label>
                  <input type="hidden" name="ca_id" value="{{ $kredit->id }}">
                  <select type="text" name="jenis_industri" class="form-control input-sm">
                      <option value="Industri Dasar Kimia dan Aneka Industri" {{ ($kredit->CustomerDetail->jenis_industri=='Industri Dasar Kimia dan Aneka Industri' ? 'selected':'')}}>Industri Dasar Kimia dan Aneka Industri</option>
                      <option value="Barang Konsumsi" {{($kredit->CustomerDetail->jenis_industri=='Barang Konsumsi' ? 'selected':'')}}>Barang Konsumsi</option>
                      <option value="Jasa" {{($kredit->CustomerDetail->jenis_industri=='Jasa' ? 'selected':'')}}>Jasa</option>
                      <option value="Perdagangan" {{($kredit->CustomerDetail->jenis_industri=='Perdagangan' ? 'selected':'')}}>Perdagangan</option>
                      <option value="Pertambangan" {{($kredit->CustomerDetail->jenis_industri=='Pertambangan' ? 'selected':'')}}>Pertambangan</option>
                      <option value="Marine" {{($kredit->CustomerDetail->jenis_industri=='Marine' ? 'selected':'')}}>Marine</option>
                      <option value="Aviasi" {{($kredit->CustomerDetail->jenis_industri=='Aviasi' ? 'selected':'')}}>Aviasi</option>
                  </select>
                </div>

                <div class="form-group col-sm-6">
                  <label>Rata-Rata Keterlambatan Customer 1 Tahun Terakhir:</label>
                  <select type="text" name="keterlambatan" class="form-control input-sm">
                      <option value="30 Hari Kalendar Setelah Tanggal Jatuh Tempo" {{ ($kredit->CustomerDetail->keterlambatan=='30 Hari Kalendar Setelah Tanggal Jatuh Tempo' ? 'selected':'')}}>30 Hari Kalendar Setelah Tanggal Jatuh Tempo</option>
                      <option value="16 Hari Kalender < x < 30 Hari Setelah Tanggal Jatuh Tempo" {{ ($kredit->CustomerDetail->keterlambatan=='16 Hari Kalender < x < 30 Hari Setelah Tanggal Jatuh Tempo' ? 'selected':'')}}>16 Hari Kalender < x < 30 Hari Setelah Tanggal Jatuh Tempo</option>
                      <option value="8 Hari Kalender < x < 15 Hari Setelah Tanggal Jatuh Tempo" {{ ($kredit->CustomerDetail->keterlambatan=='8 Hari Kalender < x < 15 Hari Setelah Tanggal Jatuh Tempo' ? 'selected':'')}}>16 Hari Kalender < x < 30 Hari Setelah Tanggal Jatuh Tempo</option>
                      <option value="1 Hari Kalender < x < 7 Hari Setelah Tanggal Jatuh Tempo" {{ ($kredit->CustomerDetail->keterlambatan=='1 Hari Kalender < x < 7 Hari Setelah Tanggal Jatuh Tempo' ? 'selected':'')}}>1 Hari Kalender < x < 7 Hari Setelah Tanggal Jatuh Tempo</option>
                      <option value="Tanggal Jatuh Tempo" {{ ($kredit->CustomerDetail->keterlambatan=='Tanggal Jatuh Tempo' ? 'selected':'')}}>Tanggal Jatuh Tempo</option>
                  </select>
                </div>
              </div>

                <hr>

              <div class="row">
                <div class="col-lg-12 margin-tb"></div>
                <div class="form-group col-md-6">
                  <label>Riwayat Restrukturisasi:</label>
                  <select type="text" name="restrukturisasi" class="form-control input-sm">
                    <option value="Pernah dan Tidak Lunas" {{($kredit->CustomerDetail->restrukturisasi=='Pernah dan Tidak Lunas' ? 'selected':'')}}>Pernah dan Tidak Lunas</option>
                    <option value="Pernah dan Berhasil (Tidak On Schedule tetapi Lunas)" {{($kredit->CustomerDetail->restrukturisasi=='Pernah dan Berhasil (Tidak On Schedule tetapi Lunas)' ? 'selected':'')}}>Pernah dan Berhasil (Tidak On Schedule tetapi Lunas)</option>
                    <option value="Tidak Pernah" {{($kredit->CustomerDetail->restrukturisasi=='Tidak Pernah' ? 'selected':'')}}>Tidak Pernah</option>
                  </select>
                </div>

                <div class="form-group col-md-6">
                  <label>Fasilitas Kredit Oleh Perbankan:</label>
                  <select type="text" name="fasilitas_kredit" class="form-control input-sm">
                    <option value="Tidak Mempunyai" {{($kredit->CustomerDetail->fasilitas_kredit=='Tidak Mempunyai' ? 'selected':'')}}>Tidak Mempunyai</option>
                    <option value="Mempunyai Fasilitas Kredit Bank Atau Tidak Mempunyai Namun Memenuhi" {{($kredit->CustomerDetail->fasilitas_kredit=='Mempunyai Fasilitas Kredit Bank Atau Tidak Mempunyai Namun Memenuhi' ? 'selected':'')}}>Mempunyai Fasilitas Kredit Bank Atau Tidak Mempunyai Namun Memenuhi</option>
                  </select>
                </div>
              </div>

              <div class="row">
                <div class="col-lg-12 margin-tb"></div>
                <div class="form-group col-md-6">
                  <label>Lama Bekerjasama dengan Pertamina:</label>
                  <select type="text" name="lama_kerjasama" class="form-control input-sm">
                    <option value="Belum Pernah" {{($kredit->CustomerDetail->lama_kerjasama=='Belum Pernah' ? 'selected':'')}}>Belum Pernah</option>
                    <option value="< 1 Tahun" {{($kredit->CustomerDetail->lama_kerjasama=='< 1 Tahun' ? 'selected':'')}}>< 1 Tahun</option>
                    <option value="1 Tahun < x < 5 Tahun" {{($kredit->CustomerDetail->lama_kerjasama=='1 Tahun < x < 5 Tahun' ? 'selected':'')}}>1 Tahun < x < 5 Tahun</option>
                    <option value="x > 5 Tahun" {{($kredit->CustomerDetail->lama_kerjasama=='x > 5 Tahun' ? 'selected':'')}}>x > 5 Tahun</option>
                  </select>
                </div>

                <div class="form-group col-md-6">
                  <label>Pelanggan Sebagai Vendor/Pemasok Pertamina:</label>
                  <select type="text" name="vendor_pemasok" class="form-control input-sm">
                    <option value="Tidak" {{($kredit->CustomerDetail->vendor_pemasok=='Tidak' ? 'selected':'')}}>Tidak</option>
                    <option value="Ya (Tidak Aktif)" {{($kredit->CustomerDetail->vendor_pemasok=='Ya (Tidak Aktif)' ? 'selected':'')}}>Ya (Tidak Aktif)</option>
                    <option value="Ya (Aktif)" {{($kredit->CustomerDetail->vendor_pemasok=='Ya (Aktif)' ? 'selected':'')}}>Ya (Aktif)</option>
                  </select>
                </div>
              </div>


                <div class="row">
                  <div class="col-lg-12 margin-tb"></div>
                  <div class="form-group col-md-6">
                    <label>Posisi Tawar Pertamina terhadap Customer:</label>
                    <select type="text" name="posisi_tawar" class="form-control input-sm">
                      <option value="Tidak Ada" {{($kredit->CustomerDetail->posisi_tawar=='Tidak Ada' ? 'selected':'')}}>Tidak Ada</option>
                      <option value="Ada" {{($kredit->CustomerDetail->posisi_tawar=='Ada' ? 'selected':'')}}>Ada</option>
                    </select>
                  </div>
               </div>

               <hr>

                <div class="row">
                  <div class="col-lg-12 margin-tb"></div>
                  <div class="form-group col-md-6">
                    <label>Badan Usaha:</label>
                    <select type="text" name="badan_usaha" class="form-control input-sm">
                      <option value="Bukan PT(Perseroan Terbatas)" {{($kredit->CustomerDetail->badan_usaha=='Bukan PT(Perseroan Terbatas)' ? 'selected':'')}}>Bukan PT(Perseroan Terbatas)</option>
                      <option value="PT Yang Tidak Terdaftar di BEI" {{($kredit->CustomerDetail->badan_usaha=='PT Yang Tidak Terdaftar di BEI' ? 'selected':'')}}>PT Yang Tidak Terdaftar di BEI</option>
                      <option value="PT Yang Tidak Terdaftar di BEI Namun Multinasional" {{($kredit->CustomerDetail->badan_usaha=='PT Yang Tidak Terdaftar di BEI Namun Multinasional' ? 'selected':'')}}>PT Yang Tidak Terdaftar di BEI Namun Multinasional</option>
                      <option value="PT yang terdaftar di BEI" {{($kredit->CustomerDetail->badan_usaha=='PT yang terdaftar di BEI' ? 'selected':'')}}>PT yang terdaftar di BEI</option>
                    </select>
                  </div>

                  <div class="form-group col-md-6">
                    <label>Afiliasi:</label>
                    <select type="text" name="afiliasi" class="form-control input-sm">
                      <option value="Tidak Berafiliasi dengan Perusahaan yang Listed di BEI" {{($kredit->CustomerDetail->afiliasi=='Tidak Berafiliasi dengan Perusahaan yang Listed di BEI' ? 'selected':'')}}>Tidak Berafiliasi dengan Perusahaan yang Listed di BEI</option>
                      <option value="Tidak Berafiliasi dengan perusahaan yang Listed di BEI namun berafiliasi dengan Perusahaan Lainnya" {{($kredit->CustomerDetail->afiliasi=='Tidak Berafiliasi dengan perusahaan yang Listed di BEI namun berafiliasi dengan Perusahaan Lainnya' ? 'selected':'')}}>Tidak Berafiliasi dengan perusahaan yang Listed di BEI namun berafiliasi dengan Perusahaan Lainnya</option>
                      <option value="Affiliasi Dengan PT yang terdaftar di BEI (Terbuka)" {{($kredit->CustomerDetail->afiliasi=='Affiliasi Dengan PT yang terdaftar di BEI (Terbuka)' ? 'selected':'')}}>Affiliasi Dengan PT yang terdaftar di BEI (Terbuka)</option>
                    </select>
                  </div>
                </div>

              <div class="row">
              <div class="form-group col-md-6">
                  <label>Kondisi Industri:</label>
                  <select type="text" name="kondisi_industri" class="form-control input-sm">
                    <option value="Menurun" {{($kredit->CustomerDetail->kondisi_industri=='Menurun' ? 'selected':'')}}>Menurun</option>
                    <option value="Stabil" {{($kredit->CustomerDetail->kondisi_industri=='Stabil' ? 'selected':'')}}>Stabil</option>
                    <option value="Bertumbuh" {{($kredit->CustomerDetail->kondisi_industri=='Bertumbuh' ? 'selected':'')}}>Bertumbuh</option>
                  </select>
                </div>

                <div class="form-group col-md-6">
                    <label>Opini Audit:</label>
                    <select type="text" name="opini_audit" class="form-control input-sm">
                      <option value="Non Audited / Disclaimer / Adverse / Review" {{($kredit->CustomerDetail->opini_audit=='Non Audited / Disclaimer / Adverse / Review' ? 'selected':'')}}>Non Audited / Disclaimer / Adverse / Review</option>
                      <option value="Audited dan Opini Wajar dengan pengecualian" {{($kredit->CustomerDetail->opini_audit=='Audited dan Opini Wajar dengan pengecualian' ? 'selected':'')}}>Audited dan Opini Wajar dengan pengecualian</option>
                      <option value="Audited dan Opini Wajar tanpa pengecualian" {{($kredit->CustomerDetail->opini_audit=='Audited dan Opini Wajar tanpa pengecualian' ? 'selected':'')}}>Audited dan Opini Wajar tanpa pengecualian</option>
                    </select>
                  </div>
              </div>


              <div class="row">
                <div class="form-group col-md-6">
                    <label>Diaudit oleh KAP terdaftar di OJK:</label>
                    <select type="text" name="audit_kap" class="form-control input-sm">
                      <option value="Tidak Terdaftar" {{($kredit->CustomerDetail->audit_kap=='Tidak Terdaftar' ? 'selected':'')}}>Tidak Terdaftar</option>
                      <option value="Terdaftar" {{($kredit->CustomerDetail->audit_kap=='Terdaftar' ? 'selected':'')}}>Terdaftar</option>
                    </select>
                  </div>
              </div>

              <div class="row">
                 <div class="col-lg-12 margin-tb"></div>
                <div class="form-group col-md-11">
                  <button type="submit" class="btn btn-primary btn-lg pull-right" style="margin-left:18px">Update</button>
                </div>
              </div>
            </form>

            </div>
          </div>
        </div>

@endsection  

@section('afterscript')
  <script src="{{ asset('beagle/lib/select2/js/select2.full.min.js') }}"></script>
  <script>
    $(".select2_demo_2").select2({
      width: '100%',
      height: '100%'
    });
  </script>
@stop
