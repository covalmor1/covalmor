@extends('layouts.app')
@section('content')


    @if (\Session::has('success'))
      <div class="alert alert-success">
        <p>{{ \Session::get('success') }}</p>
      </div><br />
     @endif

    <div class="row">
      <div class="col-lg-12 margin-tb"></div>
          @if (Auth::user()->isRole('user'))
          <div class="form-group col-md-1">
            <div class="nav navbar-right">
               <a href="{{ url ('/Kredit/create') }}" class="btn btn-success btn-sm">+ Request</a>
            </div>
          </div>
          @endif

      <div class="col-md-1 pull-right">
          <a data-toggle="modal" class="btn btn-sm btn-warning" href="#modal-form">Filter</a>
      </div>
    </div>
    <br>

<div class="panel panel-default panel-border-color panel-border-color-danger">
                <div class="panel-heading panel-heading-divider">Credit Approval List</div>
                <div class="panel-body">
    <div class="table-responsive">
    <table id="table3" class="table table-striped">
    <thead>
      <tr>
        <th data-visible="false">No Tiket</th>
        <th>Customer</th>
        <th>Jenis Produk</th>
        <th>Perkiraan Volume</th>
        <th data-visible="false">UOM</th>
        <th>Perkiraan Nilai Transaksi</th>
        <th data-visible="false">Mekanisme Pembayaran</th>
        <th data-visible="false">Bentuk Jaminan</th>
        <th data-visible="false">Sanksi Keterlambatan</th>
        <th data-visible="false">Syarat Penyerahan</th>
        <th data-visible="false">Jangka Waktu Mulai</th>
        <th data-visible="false">Jangka Waktu Berakhir</th>
        <th>Credit Limit</th>
        <th>Creator</th>
        <th>Status</th> 
        <th data-visible="false">Tanggal Create User</th>
        <th data-visible="false">Tanggal Submit AR</th>
        <th data-visible="false">Tanggal Submit Cashbank</th>
        <th data-visible="false">Tanggal Submit FBS</th>
        <th data-visible="false">Tanggal Submit Manajemen Resiko</th>
        <th data-visible="false">Tanggal Submit Komite Kredit</th>
        <th></th>
        <th></th>
      </tr>
    </thead>
     <tbody>
      @foreach ($kredit as $key)
      <tr>
        <td data-visible="false">{{ $key->no_tiket }}</td>
        <td>{{ $key->Customer->customer_no }} {{ $key->Customer->name }}</td>
        <td>
          @foreach ($key->Product as $no => $value)
            @if ($no!=0)
              , 
            @endif
              {{$value->Detail->material_name}}
          @endforeach
        <td>{{ number_format($key->volume) }}</td>
        <td data-visible="false">{{ $key->satuan }}</td>
        <td>Rp. {{ number_format($key->nilai_transaksi) }}</td>
        <td data-visible="false">{{ $key->pembayaran }}</td>
        <td data-visible="false">{{ $key->jaminan }}</td>
        <td data-visible="false">{{ ($key->flag_denda=='1' ? 'Ya':'Tidak') }}</td>
        <td data-visible="false">{{ $key->syarat_penyerahan }}</td>
        <td data-visible="false">{{ $key->tempo_start }}</td>
        <td data-visible="false">{{ $key->tempo_end }}</td>
        <td>Rp. {{ number_format($key->credit_limit) }}</td>
        <td>{{ $key->Creator->name }}</td>
        <td><a style="pointer-events: none;cursor: default;" class="btn btn-sm btn-{{ $key->Status->class }}"> {{ $key->Status->name }}</a></td>
        <td data-visible="false">{{ $key->created_at }}</td>
        <td data-visible="false">{{ $key->submit_ar_date }}</td>
        <td data-visible="false">{{ $key->submit_cb_date }}</td>
        <td data-visible="false">{{ $key->submit_fbs_date }}</td>
        <td data-visible="false">{{ $key->submit_mr_date }}</td>
        <td data-visible="false">{{ $key->submit_kk_date }}</td>
        <td><a href="{{action('Kredit\KreditController@show', $key->id )}}" title="View"><span class="btn btn-sm btn-primary"><i class="mdi mdi-eye"></i></span></a></td>
          @if (Auth::user()->isRole('user'))
          <td></td>
            <!-- <td>   <a href="{{action('Kredit\KreditController@edit', $key->id )}}" title="Edit"><span class="icon mdi mdi-edit"></span></a>
            </td>
            <td>
              <form action="{{action('Kredit\KreditController@destroy', $key->id )}}" method="post">
              {{csrf_field()}}
              <input name="_method" type="hidden" value="DELETE">
              <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
              <button class="icon mdi mdi-delete" style="background: transparent;border:none;color: #4285f4;" title="Delete" onclick="return confirm('Apakah Anda Ingin Hapus..');" class="icon"></button>
            </form>
          </td> -->
        @elseif (Auth::user()->isRole('ar'))
          @if (!isset($key->CustomerDetail))
          <td><a href="{{action('Kredit\CustomerDetailController@add', $key->id )}}" title="Input Identitas Customer"><span class="btn btn-sm btn-warning"><i class="mdi mdi-file-plus"></i></span></a></td>
          @else
          <td></td>
          @endif
        @elseif (Auth::user()->isRole('cashbank'))
          @if (is_null($key->bank_garansi_doc))
          <td><a href="{{action('Kredit\CustomerDetailController@upload', $key->id )}}" title="Upload Cash Bank"><span class="icon mdi mdi-upload"></span></a></td>
          @else
          <td></td>
          @endif
        @elseif (Auth::user()->isRole('fbs'))
          @if (!isset($key->Nota))
          <td><a href="{{action('Kredit\NotaController@entry', $key->id )}}" title="Entry Nota Pengantar"><span class="icon mdi mdi-file-plus"></span></a></td>
          @elseif (isset($key->Nota) AND is_null($key->credit_scoring_doc))
          <td><a href="{{action('Kredit\CustomerDetailController@form_cs', $key->id )}}" title="Upload Kertas Kerja Credit Scoring"><span class="icon mdi mdi-upload"></span></a></td>
          @else
          <td></td>
          @endif

        @elseif (Auth::user()->isRole('administrator'))
          <td>
            <form action="{{action('Kredit\KreditController@destroy', $key->id )}}" method="post">
            {{csrf_field()}}
            <input name="_method" type="hidden" value="DELETE">
            <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
            <button class="btn btn-sm btn-danger" title="Delete" onclick="return confirm('Apakah Anda Ingin Hapus..');" class="icon"><i class="mdi mdi-delete"></i></button>
          </form>
        </td>
        @else
        <td></td>
        @endif
      </tr>
      @endforeach
      
    </tbody>
  
  </table>
</div>
</div>
</div>

          <div id="modal-form" class="modal fade" aria-hidden="true">
              <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-sm-2">
                            <h3 class="m-t-none m-b">Filter By:</h3>
                            </div>
                            <div class="col-sm-10">

                                <form role="form" method="post" action="{{ action('Kredit\KreditController@filter') }}">
                                  {{csrf_field()}}
                                  <div class="row">
                                    <div class="form-group col-sm-12">
                                      <label class="font-normal">Status</label>
                                              <select type="text" name="status" class="form-control input-sm">
                                                <option value="%">All</option>
                                                <option value="0">Draft User</option>
                                                <option value="1">AR</option>
                                                <option value="2">Cash Bank</option>
                                                <option value="3">FBS</option>
                                                <option value="4">Manajemen Resiko</option>
                                                <option value="5">Komite Kredit</option>
                                                <option value="6">Completed</option>
                                              </select>
                                        </div>
                                      </div>
                                      <hr>
                                        <div class="row">
                                        <div class="form-group col-sm-12" id="data_1">
                                      <label class="font-normal">Dari Tanggal</label>
                                            <div class="input-group date">
                                              <span class="input-group-addon"></span><input type="text" class="form-control input-sm" name="from_tanggal" value="{{Carbon\Carbon::today()->format('Y-m-d')}}">
                                             </div>
                                        </div>
                                      </div>
                                      <hr>
                                        <div class="row">
                                        <div class="form-group col-sm-12" id="data_1">
                                      <label class="font-normal">Ke Tanggal</label>
                                            <div class="input-group date">
                                              <span class="input-group-addon"></span><input type="text" class="form-control input-sm" name="to_tanggal" value="{{Carbon\Carbon::today()->format('Y-m-d')}}">
                                             </div>
                                        </div>
                                      </div>
                                        <hr>
                                    <div>
                                        <input class="btn btn-md btn-primary pull-right m-t-n-xs" type="submit" name="filter" value="Filter">
                                        <label> 
                                    </div>
                                </form>
                        </div>
                    </div>
                </div>
                </div>
            </div>
    </div>
 @endsection   