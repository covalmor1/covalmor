@extends('layouts.app')

@section('afterhead')
    <link href="{{ asset('beagle/lib/select2/css/select2.min.css') }}" rel="stylesheet">
@endsection

@section('content')
      @if ($errors->any())
      <div class="alert alert-danger">
          <ul>
              @foreach ($errors->all() as $error)
                  <li>{{ $error }}</li>
              @endforeach
          </ul>
      </div><br />
      @endif
      @if (\Session::has('success'))
      <div class="alert alert-success">
          <p>{{ \Session::get('success') }}</p> 
      </div><br />
      @endif

        <div class="row">
            <div class="col-sm-12">
              <div class="panel panel-default panel-border-color panel-border-color-danger">
                <div class="panel-heading panel-heading-divider">Form Nota</div>
                <div class="panel-body">
            <form method="post" action="{{action('Kredit\NotaController@nota_save', $kredit->id)}}" enctype="multipart/form-data">
              {{csrf_field()}}
              <div class="row">
                <div class="col-lg-12 margin-tb"></div>

                <div id="accordion1" class="panel-group accordion">
                <div class="panel panel-default">
                  <div class="panel-heading">
                    <h4 class="panel-title"><a data-toggle="collapse" data-parent="#accordion1" href="#collapseOne"><i class="icon mdi mdi-chevron-down"></i> {{ $kredit->Customer->name}}</a></h4>
                  </div>
                  <div id="collapseOne" class="panel-collapse collapse in">
                    <div class="panel-body">
                      <div class="row">
                        <div class="form-group col-sm-6">
                          <label>Kode Customer MySAP:</label>
                          <input type="text" name="customer_no" class="form-control input-sm" value="{{ $kredit->Customer->customer_no }}" readonly>
                        </div>

                        <div class="form-group col-sm-6">
                          <label>NPWP:</label>
                          <input type="text" name="customer_no" class="form-control input-sm" value="{{ $kredit->Customer->npwp }}" readonly>
                        </div>
                      </div>

                      <div class="row">
                       <div class="form-group col-sm-6">
                          <label>Alamat:</label>
                          <input type="text" name="jenis_jaminan" class="form-control input-sm" value="{{ $kredit->Customer->address }}" readonly>
                        </div>
                         
                        <div class="form-group col-sm-6">
                          <label>Bentuk Jaminan:</label>
                          <input type="text" name="jenis_jaminan" class="form-control input-sm" value="{{ $kredit->jaminan }}" readonly>
                        </div>

                      </div>
                    </div>
                  </div>
                </div>

                      <hr>
              <div class="col-lg-12">
              <div class="row" id="data_1">
                <div class="col-lg-12 margin-tb"></div>
                <div class="form-group col-md-4 date">
                    <label>Tanggal Nota</label>
                      <div class="col-md-12 input-group date">
                          <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                          <input type="text" class="form-control input-sm" name="tanggal_nota" value="{{Carbon\Carbon::today()->format('Y-m-d')}}">
                        </div>
                    </div>
                  </div>

              <div class="row">
                <div class="form-group col-sm-8">
                  <label>Perihal:</label>
                  <input type="hidden" name="ca_id" value="{{ $kredit->id }}">
                  <input type="text" name="perihal" class="form-control">
                </div>
              </div>

                <div class="row">
                <div class="form-group col-sm-8">
                  <label>Isi:</label>
                  <textarea name="isi" class="form-control" rows="4"></textarea>
                </div>
              </div>

              <div class="row">
                 <div class="col-lg-12 margin-tb"></div>
                <div class="form-group col-md-11">
                  <button type="submit" class="btn btn-primary btn-lg pull-right" style="margin-left:18px">Save</button>
                </div>
              </div>

                     </div>
                  </div>
                </div>
                </div>


                <hr>

              
            </form>

            </div>
          </div>

@endsection  

@section('afterscript')
  <script src="{{ asset('beagle/lib/select2/js/select2.full.min.js') }}"></script>
  <script>
    $(".select2_demo_2").select2({
      width: '100%',
      height: '100%'
    });
  </script>
@stop
