@extends('layouts.app')
@section('content')
      
    @if (\Session::has('success'))
      <div class="alert alert-success">
        <p>{{ \Session::get('success') }}</p>
      </div><br />
     @endif

      @if ($errors->any())
      <div class="alert alert-danger">
          <ul>
              @foreach ($errors->all() as $error)
                  <li>{{ $error }}</li>
              @endforeach
          </ul>
      </div><br />
      @endif

    <div class="row">
		 <div class="col-lg-12 margin-tb"></div>
		 <div class="form-group col-md-1">
			<div class="nav navbar-left">
			   <a href="{{ url('Kredit') }}" class="btn btn-default btn-md"><span class="mdi mdi mdi-long-arrow-return"></span> Back</a>
			</div>
		</div>
	</div>

      <div class="row">
      	<div class="panel panel-default">
                <div class="panel-heading">Credit Approval Detail {{ $kredit->no_tiket }}</div>
                
                <div class="tab-container">
                  <ul class="nav nav-tabs nav-tabs-danger">
                    <li class="active"><a href="#user" data-toggle="tab">Pengajuan User</a></li>
                    @if (isset($kredit->CustomerDetail))
                    <li><a href="#account-receivable" data-toggle="tab">Identitas Customer</a></li>
                    @endif
                    @if ($kredit->Nota)
                    <li><a href="#nota-pengantar" data-toggle="tab">Nota Pengantar</a></li>
                    @endif
                  </ul>
                  <div class="tab-content">
                    <div id="user" class="tab-pane active cont">
                      <div class="user-info-list panel panel-default">
                  <div class="panel-body">

                    <table class="no-border no-strip skills">
                      <tbody class="no-border-x no-border-y">
                        <tr>
                        	<td class="item">Customer No</td>
                          	<td class="item">: {{ $kredit->Customer->customer_no}}</td>
                          	<td class="item">Customer Name</td>
                          	<td class="item">: {{ $kredit->Customer->name}}</td>
                        </tr>
                        <tr>
                        	<td class="item">NPWP</td>
                          	<td class="item">: {{ $kredit->Customer->npwp}}</td>
                          	<td class="item">Alamat</td>
                          	<td class="item">: {{ $kredit->Customer->address}}</td>
                        </tr>
                        <tr>
                     
                      </tbody>
                    </table>

                    <hr>
                    <table class="no-border no-strip skills">
                      <tbody class="no-border-x no-border-y">
                        </tr>
                        <tr>
                          <td class="item">No Surat</span></td>
                          <td class="item">: <?php echo str_replace(" ", "&nbsp;", $kredit->no_surat); ?></td>
                          <td></td>
                        </tr>
                        <tr>
                        	<td class="item">Jangka Waktu Credit Approval</span></td>
                          <td class="item">: {{ date('d/m/Y', strtotime($kredit->tempo_start)) }} s.d. {{ date('d/m/Y', strtotime($kredit->tempo_end)) }}</td>
                          <td></td>
                        </tr>
                        <tr>
                        	<td class="item">Jenis Produk</td>
                          <td>: 
                          	@foreach ($kredit->Product as $no => $value)
          				            @if ($no!=0)
          				              , 
          				            @endif
          				              {{$value->Detail->material_name}}
          				          @endforeach
					      </td>
					      <td></td>
                        </tr>
                        <tr>
                        	<td class="item">Perkiraan Volume</span></td>
                          <td class="item">: {{ number_format($kredit->volume) }} {{ number_format($kredit->satuan) }} per bulan</td>
                          <td></td>
                        </tr>
                        <tr>
                        	<td class="item">Perkiraan Nilai Transaksi</span></td>
                          <td class="item">: Rp. {{ number_format($kredit->nilai_transaksi) }}</td>
                          <td></td>
                        </tr>
                        <tr>
                        	<td class="item">Credit Limit</span></td>
                          <td class="item">: Rp. {{ number_format($kredit->credit_limit) }}</td>
                          <td></td>
                        </tr>
                        <tr>
                        	<td class="item">Mekanisme Pembayaran</span></td>
                          <td class="item">: {{ $kredit->pembayaran }}</td>
                          <td></td>
                        </tr>
                        <tr>
                        	<td class="item">Bentuk Jaminan</span></td>
                          <td class="item">: {{ $kredit->jaminan }}</td>
                          <td></td>
                        </tr>
                        <tr>
                        	<td class="item">Sanksi Keterlambatan</span></td>
                          <td class="item">: {{ ($kredit->flag_denda=='1' ? 'Ya':'Tidak') }}</td>
                          <td></td>
                        </tr>

                        <tr>
                        	<td class="item">Syarat Penyerahan</span></td>
                          <td class="item">: {{ $kredit->syarat_penyerahan }}</td>
                          <td></td>
                        </tr>
                      </tbody>
                    </table>

                    <hr>

                    <table class="no-border no-strip skills">
                      <tbody class="no-border-x no-border-y">
                        <tr>
                          <td class="item">Memo Pengantar</td>
                            <td class="item"><a href="{{ asset($kredit->memo_pengantar) }}">download</a></td>
                        </tr>
                        
                        <tr>
                            <td class="item">Dokumen Pendukung</td>
                            <td>
                              <ol style="margin-left: -26px;">
                                @if ($kredit->doc_lka!='')
                                <li><a href="{{ asset($kredit->doc_lka) }}">Laporan Keuangan Audit</a></li>
                                @endif
                                @if ($kredit->doc_cas!='')
                                <li><a href="{{ asset($kredit->doc_cas) }}">Credit Approval Sebelumnya</a></li>
                                @endif
                                @if ($kredit->doc_bg!='')
                                <li><a href="{{ asset($kredit->doc_bg) }}">Bank Garansi</a></li>
                                @endif
                                @if ($kredit->doc_pml!='')
                                <li><a href="{{ asset($kredit->doc_pml) }}">Pocket Margin dan Lainnya</a></li>
                                @endif
                              </ol>
                            </td>
                        </tr>
                        
                        <tr>
                          @if (!is_null($kredit->bank_garansi_doc))
                            <td class="item">Konfirmasi BG / Surat Jaminan</td>
                              <td class="item"><a href="{{ asset($kredit->bank_garansi_doc) }}">download</a></td>
                          @endif

                           @if (!is_null($kredit->bank_konfirmasi_doc))
                            <td class="item">Konfirmasi dari Bank</td>
                              <td class="item"><a href="{{ asset($kredit->bank_konfirmasi_doc) }}">download</a></td>
                          @endif

                        </tr>
                        <tr>
                         @if (!is_null($kredit->credit_scoring_doc))
                            <td class="item">Credit Scoring</td>
                              <td class="item"><a href="{{ asset($kredit->credit_scoring_doc) }}">download</a></td>
                          @endif
                        </tr>
                        <tr>
                        	<td class="item"><a class="btn btn-xl btn-warning" href="{{ url('Pdf/CreditApproval/'.$kredit->id.'') }}"><span class="mdi mdi-cas-download"></span> Download Credit Approval</a></td>
                          @if (!is_null($kredit->credit_approval_doc))
                          <td class="item"><a class="btn btn-xl btn-primary" href="{{ asset($kredit->credit_approval_doc) }}"><span class="mdi mdi-cas-download"></span> Download Signed Credit Approval</a></td>
                          @endif
                        </tr>

                      </tbody>
                    </table>

                  </div>
                </div>
               </div>
                    <div id="account-receivable" class="tab-pane cont">
                      <div class="user-info-list panel panel-default">
                     <div class="panel-body">

                    <table class="no-border no-strip skills">
                      <tbody class="no-border-x no-border-y">
                        </tr>
                        <tr>
                          <td class="item">Jenis Industri</span></td>
                          <td class="item">: {{ $kredit->CustomerDetail->jenis_industri}}</td>
                          <td></td>
                        </tr>
                        <tr>
                          <td class="item">Rata-Rata Keterlambatan Customer 1 Tahun Terakhir</td>
                          <td>: {{ $kredit->CustomerDetail->keterlambatan }}</td>
                          <td></td>
                        </tr>
                        <tr>
                          <td class="item">Riwayat Restrukturisasi</span></td>
                          <td>: {{ $kredit->CustomerDetail->restrukturisasi }}</td>
                          <td></td>
                        </tr>
                        <tr>
                          <td class="item">Fasilitas Kredit Oleh Perbankan</span></td>
                          <td>: {{ $kredit->CustomerDetail->fasilitas_kredit }}</td>
                          <td></td>
                        </tr>
                        <tr>
                          <td class="item">Lama Bekerjasama dengan Pertamina</span></td>
                          <td>: {{ $kredit->CustomerDetail->lama_kerjasama }}</td>
                          <td></td>
                        </tr>
                        
                        <tr>
                          <td class="item">Pelanggan Sebagai Vendor/Pemasok Pertamina</span></td>
                          <td class="item">: {{ $kredit->CustomerDetail->vendor_pemasok }}</td>
                          <td></td>
                        </tr>
                        <tr>
                          <td class="item">Posisi Tawar Pertamina terhadap Customer</span></td>
                          <td class="item">: {{ $kredit->CustomerDetail->posisi_tawar }}</td>
                          <td></td>
                        </tr>
                        <tr>
                          <td class="item">Badan Usaha</span></td>
                          <td class="item">: {{ $kredit->CustomerDetail->badan_usaha }}</td>
                          <td></td>
                        </tr>

                        <tr>
                          <td class="item">Affiliasi</span></td>
                          <td class="item">: {{ $kredit->CustomerDetail->afiliasi }}</td>
                          <td></td>
                        </tr>
                        <tr>
                          <td class="item">Kondisi Industri</span></td>
                          <td class="item">: {{ $kredit->CustomerDetail->kondisi_industri }}</td>
                          <td></td>
                        </tr>
                        <tr>
                          <td class="item">Opini Audit</span></td>
                          <td class="item">: {{ $kredit->CustomerDetail->opini_audit }}</td>
                          <td></td>
                        </tr>
                        <tr>
                          <td class="item">Diaudit oleh KAP terdaftar di OJK</span></td>
                          <td class="item">: {{ $kredit->CustomerDetail->audit_kap }}</td>
                          <td></td>
                        </tr>
                        @if (isset($kredit->CustomerDetail))
                        <tr>
                          <td class="item"><a class="btn btn-xl btn-warning" href="{{ url('Pdf/IdentitasCustomer/'.$kredit->CustomerDetail->id.'') }}"><span class="mdi mdi-cas-download"></span> Download Identitas Customer</a></td>
                        </tr>
                        @endif
                      </tbody>
                    </table>

                      </div>
                    </div>
                  </div>
                    <div id="nota-pengantar" class="tab-pane">

                    <div class="user-info-list panel panel-default">
                     <div class="panel-body">

                      <div class="row">
                        <div class="col-lg-2">
                          <label>Tanggal Nota</label>
                        </div>
                        <div class="col-lg-10">
                          <label>:{{ $kredit->Nota->tanggal_nota }}</label>
                        </div>
                      </div>

                      <hr>

                      <div class="row">
                        <div class="col-lg-2">
                          <label>Perihal</label>
                        </div>
                        <div class="col-lg-10">
                          <label>:{{ $kredit->Nota->perihal }}</label>
                        </div>
                      </div>

                      <hr>

                      <div class="row">
                        <div class="col-lg-2">
                          <label>Isi</label>
                        </div>
                        <div class="col-lg-10">
                          <label>:{{ $kredit->Nota->isi }}</label>
                        </div>
                      </div>

                      <hr>
                      
                      @if ($kredit->Nota)
                      <div class="row">
                        <div class="col-md-4">
                          <a class="btn btn-xl btn-warning" href="{{ url('Pdf/Nota/'.$kredit->id.'') }}"><span class="mdi mdi-cas-download"></span> Download Nota Pengantar</a>
                        </div>
                      </div>
                      @endif

                          </div>
                        </div>

                    </div>
              @if (count($kredit->Note)>0)
                <div class="row">
                  <div class="col-md-12">
                    <div id="accordion1" class="panel-group accordion">
                    <div class="panel panel-default">
                      <div class="panel-heading">
                        <h4 class="panel-title" style="background: #f4f1f1;"><a data-toggle="collapse" data-parent="#accordion1" href="#collapseOne"><i class="icon mdi mdi-chevron-down"></i> Notes</a></h4>
                      </div>
                      <div id="collapseOne" class="panel-collapse collapse">
                        <div class="panel-body">
                            <table class="table table-condensed">
                              <tbody>
                                  @foreach ($kredit->Note as $no => $key)
                                  <tr class="bg-warning">
                                    <th colspan="2">#{{ ($no+1) }}</th>
                                    <td colspan="2">
                                       @if ($key->action_type=='0')
                                          <span class="btn-sm btn-danger">Reject</span>
                                        @else
                                          <span class="btn-sm btn-success">Approve</span>
                                        @endif
                                    </td>
                                  </tr>
                                    <tr>
                                      <td>User</span></td>
                                      <td>:{{ $key->Creator->name}}</td>
                                      <td>Tanggal</span></td>
                                      <td>:{{ $key->created_at}}</td>
                                    </tr>
                                    <tr>
                                      <td>Catatan</span></td>
                                      <td colspan="3">{{ $key->action_note}}</td>
                                    </tr>
                                    @endforeach
                              </tbody>
                            </table>
                          </div>
                        </div>
                      </div>
                    </div>
                    </div>
                  </div>
                @endif
                    <div class="row">
                      <div class="col-lg-9">
                        
                      </div>
                      @if (Auth::user()->isRole('user') AND $kredit->status=='0')
                      <div class="col-lg-3 pull-right">
                        <a href="{{action('Kredit\KreditController@edit', $kredit->id )}}" class="btn btn-md btn-warning"><span class="mdi mdi-edit"></span> Edit</a>
                        <form method="post" action="{{ url('/Kredit/user/submit/'.$kredit->id.'/'.($kredit->status+1).'') }}" class="btn btn-md">
                          {{csrf_field()}}
                          <button data-toggle="modal" data-target="#approve" type="button" class="btn btn-md btn-primary"><span class="mdi mdi-check-all"></span> Submit</button>
                          </form
                      </div>
                      @elseif (Auth::user()->isRole('ar') AND $kredit->status=='1')
                        @if (!isset($kredit->CustomerDetail))
                        <div class="col-lg-3 pull-right">
                          <button data-toggle="modal" data-target="#reject" type="button" class="btn btn-md btn-danger"><span class="mdi mdi-close"></span> Reject</button>
                          <a href="{{ url('/Kredit/ar/form/'.$kredit->id.'') }}" class="btn btn-md btn-primary"><span class="mdi mdi-plus"></span> Identitas Customer</a>
                        </div>
                        @else
                        <a href="{{action('Kredit\CustomerDetailController@edit', $kredit->id )}}" class="btn btn-md btn-warning"><span class="mdi mdi-edit"></span> Edit</a>
                        <form method="post" action="{{ url('/Kredit/user/submit/'.$kredit->id.'/'.($kredit->status+1).'') }}" class="btn btn-md">
                          {{csrf_field()}}
                          <button data-toggle="modal" data-target="#approve" type="button" class="btn btn-md btn-primary"><span class="mdi mdi-check-all"></span> Submit</button>
                          </form>
                        @endif
                      @elseif (Auth::user()->isRole('cashbank') AND $kredit->status=='2')
                        @if (is_null($kredit->bank_garansi_doc))
                        <div class="col-lg-3 pull-right">
                          <button data-toggle="modal" data-target="#reject" type="button" class="btn btn-md btn-danger"><span class="mdi mdi-close"></span> Reject</button>
                          <a href="{{ url('/Kredit/cashbank/form/'.$kredit->id.'') }}" class="btn btn-md btn-primary"><span class="mdi mdi-plus"></span> Dokumen Bank</a>
                        </div>
                        @else
                        <a href="{{action('Kredit\CustomerDetailController@cb_edit', $kredit->id )}}" class="btn btn-md btn-warning"><span class="mdi mdi-edit"></span> Edit</a>
                        <form method="post" action="{{ url('/Kredit/user/submit/'.$kredit->id.'/'.($kredit->status+1).'') }}" class="btn btn-md">
                          {{csrf_field()}}
                          <button data-toggle="modal" data-target="#approve" type="button" class="btn btn-md btn-primary"><span class="mdi mdi-check-all"></span> Submit</button>
                          </form>
                        @endif
                      @elseif (Auth::user()->isRole('fbs') AND $kredit->status=='3')
                        @if (!isset($kredit->Nota) OR is_null($kredit->credit_scoring_doc))
                        <div class="col-lg-3 pull-right">
                          @if (!isset($kredit->Nota))
                          <button data-toggle="modal" data-target="#reject" type="button" class="btn btn-md btn-danger"><span class="mdi mdi-close"></span> Reject</button>
                          <a href="{{ url('/Kredit/Fbs/entry/'.$kredit->id.'') }}" class="btn btn-md btn-primary"><span class="mdi mdi-plus"></span> Nota Pengantar</a>
                          @elseif (isset($kredit->Nota) AND is_null($kredit->credit_scoring_doc))
                          <a href="{{action('Kredit\CustomerDetailController@fbs_edit', $kredit->id )}}" class="btn btn-md btn-warning"><span class="mdi mdi-edit"></span> Edit</a>
                          <a href="{{ url('/Kredit/Fbs/form/'.$kredit->id.'') }}" class="btn btn-md btn-primary"><span class="mdi mdi-plus"></span> Credit Scoring</a>
                          @endif
                        </div>
                        @else
                        <a href="{{action('Kredit\CustomerDetailController@fbs_edit', $kredit->id )}}" class="btn btn-md btn-warning"><span class="mdi mdi-edit"></span> Edit</a>
                        <form method="post" action="{{ url('/Kredit/user/submit/'.$kredit->id.'/'.($kredit->status+1).'') }}" class="btn btn-md">
                          {{csrf_field()}}
                          <button data-toggle="modal" data-target="#approve" type="button" class="btn btn-md btn-primary"><span class="mdi mdi-check-all"></span> Submit</button>
                          </form>
                        @endif
                      @elseif (Auth::user()->isRole('manajemenresiko') AND $kredit->status=='4')
                      <div class="col-lg-3 pull-right">
                        <button data-toggle="modal" data-target="#reject" type="button" class="btn btn-md btn-danger"><span class="mdi mdi-close"></span> Reject</button>
                        <button data-toggle="modal" data-target="#approve" type="button" class="btn btn-md btn-primary"><span class="mdi mdi-check-all"></span> Approve</button>
                      </div>
                      @elseif (Auth::user()->isRole('komitekredit') AND $kredit->status=='5')
                        @if ($kredit->credit_approval_doc==null)
                        <div class="col-lg-3 pull-right">
                          <button data-toggle="modal" data-target="#reject" type="button" class="btn btn-md btn-danger"><span class="mdi mdi-close"></span> Reject</button>
                          <a href="{{ url('/Kredit/kk/form/'.$kredit->id.'') }}" class="btn btn-md btn-primary"><span class="mdi mdi-upload"></span> Upload Credit Approval</a>
                        </div>
                        @else
                        <div class="col-lg-3 pull-right">
                          <a href="{{action('Kredit\CustomerDetailController@kk_edit', $kredit->id )}}" class="btn btn-md btn-warning"><span class="mdi mdi-edit"></span> Edit</a>
                          <button data-toggle="modal" data-target="#approve" type="button" class="btn btn-md btn-primary"><span class="mdi mdi-check-all"></span> Complete</button>
                        </div>
                        @endif
                      @endif
                    </div>
                </div>
               
                </div>
              </div>
            </div>

    <div id="approve" tabindex="-1" role="dialog" class="modal fade colored-header colored-header-primary">
      <div class="modal-dialog custom-width">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" data-dismiss="modal" aria-hidden="true" class="close md-close"><span class="mdi mdi-close"></span></button>
            <h3 class="modal-title">Approve Credit {{ $kredit->Customer->name }}</h3>
            <small>Nomor tiket: {{ $kredit->no_tiket }}</small>
          </div>
          <form method="post" action="{{ url('/Kredit/Mr/approve/'.$kredit->id.'/'.($kredit->status+1).'') }}">
            {{csrf_field()}}
          <div class="modal-body">
            <div class="form-group">
              <label>Catatan</label>
              <textarea class="form-control" name="note"></textarea>
            </div>
          </div>
          <div class="modal-footer">
            <input type="submit" class="btn btn-primary md-close" value="Confirm">
          </div>
          </form>
        </div>
      </div>
    </div>

    <div id="reject" tabindex="-1" role="dialog" class="modal fade colored-header colored-header-danger">
      <div class="modal-dialog custom-width">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" data-dismiss="modal" aria-hidden="true" class="close md-close"><span class="mdi mdi-close"></span></button>
            <h3 class="modal-title">Reject Credit {{ $kredit->Customer->name }}</h3>
          </div>
          <form method="post" action="{{ url('/Kredit/Mr/reject/'.$kredit->id.'/'.($kredit->status-1).'') }}">
            {{csrf_field()}}
          <div class="modal-body">
            <div class="form-group">
              <label>Catatan Reject</label>
              <textarea class="form-control" name="note"></textarea>
            </div>
          </div>
          <div class="modal-footer">
            <input type="submit" class="btn btn-danger md-close" value="Confirm">
          </div>
          </form>
        </div>
      </div>
    </div>

      @endsection 