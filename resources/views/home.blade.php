@extends('layouts.app')

@section('content')
          <div class="row">
            <div class="col-xs-12 col-md-6 col-lg-3">
                        <div class="widget widget-tile">
                          
                          <div class="data-info">
                            <div class="desc">Pending Kredit</div>
                            <div class="value"><a href="{{ action('Kredit\KreditController@index', 'status='.(Auth::user()->id-2).'') }}"><span data-toggle="counter" data-end="{{$kredit_pending}}" class="number">{{$kredit_pending}}</span></a>
                            </div>
                          </div>
                        </div>
            </div>
            <div class="col-xs-12 col-md-6 col-lg-3">
                        <div class="widget widget-tile">
                          
                          <div class="data-info">
                            <div class="desc">Submited Credit</div>
                            <div class="value"><span data-toggle="counter" data-end="{{$kredit_submited}}" class="number">{{$kredit_submited}}</span>
                            </div>
                          </div>
                        </div>
            </div>
            <div class="col-xs-12 col-md-6 col-lg-3">
                        <div class="widget widget-tile">
                          
                          <div class="data-info">
                            <div class="desc">Progress Credit</div>
                            <div class="value"><span data-toggle="counter" data-end="{{$kredit_progress}}" class="number">{{$kredit_progress}}</span>
                            </div>
                          </div>
                        </div>
            </div>
            <div class="col-xs-12 col-md-6 col-lg-3">
                        <div class="widget widget-tile">
                          
                          <div class="data-info">
                            <div class="desc">Completed Credit</div>
                            <div class="value"><span data-toggle="counter" data-end="{{$kredit_completed}}" class="number">{{$kredit_completed}}</span>
                            </div>
                          </div>
                        </div>
            </div>
          </div>

    <div class="row">
      <div class="col-md-6">
          <div class="panel panel-default">
            <div class="panel-heading panel-heading-divider">
                 <span class="title">Status Credit</span>
                </div>
            <div class="panel-body">
              <div id="donut-chart" style="height: 250px;"></div>
            </div>
          </div>
        </div>

        <div class="col-md-6">
              <div class="panel panel-default panel-table">
                <div class="panel-heading"> 
                  <div class="title">Pending Kredit</div>
                </div>
                <div class="panel-body table-responsive">
                  <table class="table table-striped table-borderless">
                    <thead>
                      <tr>
                        <th>Customer</th>
                        <th>Perkiraan Nilai Transaksi</th>
                      </tr>
                    </thead>
                    <tbody class="no-border-x">
                      @if (count($pending_list)>0)
                        @foreach ($pending_list as $key)
                          <tr>
                            <td><a href="{{ url('Kredit/'.$key->id.'') }}">{{ str_limit($key->Customer->name, 28) }}</a></td>
                            <td>Rp. {{ number_format($key->nilai_transaksi) }}</td>
                          </tr>
                         @endforeach
                      @else
                      <tr>
                        <td colspan="2">No Data</td>
                      </tr>
                      @endif
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
    </div>

    <div class="row">
            <div class="col-md-6">
              <div class="panel panel-default panel-table">
                <div class="panel-heading"> 
                  <div class="title">Kredit Akan Jatuh Tempo</div>
                </div>
                <div class="panel-body table-responsive">
                  <table class="table table-striped table-borderless">
                    <thead>
                      <tr>
                        <th>Customer</th>
                        <th>Perkiraan Nilai Transaksi</th>
                        <th>Jangka Waktu Akhir</th>
                      </tr>
                    </thead>
                    <tbody class="no-border-x">
                      @if (count($list_tempo)>0)
                        @foreach ($list_tempo as $key)
                          <tr>
                            <td><a href="{{ url('Kredit/'.$key->id.'') }}">{{ str_limit($key->Customer->name, 28) }}</a></td>
                            <td>Rp. {{ number_format($key->nilai_transaksi) }}</td>
                            <td>{{ date('d/m/Y', strtotime($key->tempo_end)) }}</td>
                          </tr>
                         @endforeach
                      @else
                      <tr>
                        <td colspan="3">No Data</td>
                      </tr>
                      @endif
                    </tbody>
                  </table>
                </div>
              </div>
            </div>

            <div class="col-md-6">
              <div class="panel panel-default panel-table">
                <div class="panel-heading"> 
                  <div class="title">Expired Credit</div>
                </div>
                <div class="panel-body table-responsive">
                  <table class="table table-striped table-borderless">
                    <thead>
                      <tr>
                        <th>Customer</th>
                        <th>Perkiraan Nilai Transaksi</th>
                        <th>Jangka Waktu Akhir</th>
                      </tr>
                    </thead>
                    <tbody class="no-border-x">
                      @if (count($expired_list)>0)
                        @foreach ($expired_list as $key)
                          <tr>
                            <td><a href="{{ url('Kredit/'.$key->id.'') }}">{{ str_limit($key->Customer->name, 28) }}</a></td>
                            <td>Rp. {{ number_format($key->nilai_transaksi) }}</td>
                            <td>{{ date('d/m/Y', strtotime($key->tempo_end)) }}</td>
                          </tr>
                         @endforeach
                      @else
                      <tr>
                        <td colspan="3">No Data</td>
                      </tr>
                      @endif
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>

          <div class="row">
            <div class="col-md-12">
              <div class="panel panel-default panel-table">
                <div class="panel-heading">
                  <div class="title">Completed Credit</div>
                </div>
                <div class="panel-body table-responsive">
                  <table class="table table-striped table-hover">
                    <thead>
                      <tr>
                        <th>Tanggal</th>
                        <th>Customer</th>
                        <th>Volume</th>
                        <th>Nilai Transaksi</th>
                        <th>Credit Limit</th>
                      </tr>
                    </thead>
                    <tbody>
                      @if (count($expired_list)>0)
                        @foreach ($expired_list as $key)
                          <tr>
                            <td>{{ date('d/m/Y', strtotime($key->submit_kk_date)) }}</td>
                            <td><a href="{{ url('Kredit/'.$key->id.'') }}">{{ str_limit($key->Customer->name, 28) }}</a></td>
                            <td>{{ number_format($key->volume) }} {{ $key->satuan }} / {{ $key->periode_volume }}</td>
                            <td>Rp. {{ number_format($key->nilai_transaksi) }}</td>
                            <td>Rp. {{ number_format($key->credit_limit) }}</td>
                          </tr>
                        @endforeach
                        @else
                      <tr>
                        <td colspan="3">No Data</td>
                      </tr>
                      @endif
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
@endsection

@section('afterscript')
<script>
 var ar = '<?php echo $count_ar ;?>';
 var cashbank = '<?php echo $count_cashbank ;?>';
 var fbs = '<?php echo $count_fbs ;?>';
 var mkeu = '<?php echo $count_mkeu ;?>';
 var gm = '<?php echo $count_gm ;?>';
 Morris.Donut({
  element: 'donut-chart',

  data: [
    {label: "AR", value: ar },
    {label: "Cash Bank", value: cashbank},
    {label: "FBS", value: fbs},
    {label: "M. Keu", value: mkeu},
    {label: "GM", value: gm},
  ],
  backgroundColor: '#ccc',
  colors: [
    '#eb5008',
    '#ab9f14',
    '#0ed145',
    '#00a8f3',
    '#3f48cc',
  ],
});

</script>
@endsection

