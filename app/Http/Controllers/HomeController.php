<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Mail\SendMailable;
use App\Models\Kredit\Kredit;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $kredit_submited=Kredit::where('status','1')->count();
        $kredit_progress=Kredit::whereIn('status', ['2','3','4','5'])->count();
        $kredit_completed=Kredit::where('status', '6')->count();
        $kredit_pending=Kredit::where('status', (Auth::user()->id-2))->count();

        $completed_list=Kredit::where('status',6)->orderBy('submit_kk_date', 'desc')->limit(5)->get();
        $pending_list=Kredit::where('status', (Auth::user()->id-2))->orderBy('submit_kk_date', 'desc')->limit(5)->get();
        
        // Counter Cart
        $count_ar=$kredit_submited;
        $count_cashbank=Kredit::where('status', 2)->count();
        $count_fbs=Kredit::where('status', 3)->count();
        $count_mkeu=Kredit::where('status', 4)->count();
        $count_gm=Kredit::where('status', 5)->count();

        // Akan Jatuh Tempo
        $from = date('Y-m-d');
        $to = date("Y-m-d", strtotime( date( "Y-m-d", strtotime( date("Y-m-d") ) ) . "+2 month" ) );
        $list_tempo=Kredit::whereBetween('tempo_end', [$from, $to])->limit(5)->get();

        // Expired in the last 2 month
        $from = date("Y-m-d", strtotime( date( "Y-m-d", strtotime( date("Y-m-d") ) ) . "-2 month" ) ); 
        $to = date('Y-m-d');
        $expired_list=Kredit::whereBetween('tempo_end', [$from, $to])->get();
        
        return view('home', compact(
            'kredit_submited',
            'kredit_progress',
            'kredit_completed',
            'kredit_pending',
            'completed_list',
            'pending_list',
            'count_ar',
            'count_cashbank',
            'count_fbs',
            'count_mkeu',
            'count_gm',
            'list_tempo',
            'expired_list'
        ));
    }

    public function mailView($id)
    {
        $data=Kredit::find($id);
        return view('emails.name', compact('data'));
    }
}
