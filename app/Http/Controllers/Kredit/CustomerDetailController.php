<?php

namespace App\Http\Controllers\Kredit;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Kredit\Kredit;
use App\Models\Master\Customer;
use App\Models\Master\Product;
use App\Models\Transaction\CaNota;
use App\Models\Kredit\CustomerDetail;
use Illuminate\Support\Facades\Mail;
use App\Mail\SendMailable;
use App\Http\Controllers\Kredit\KreditController;
use PDF;

class CustomerDetailController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }
// Account Receivable (AR)
    public function add($id) 
    {
      $kredit = Kredit::find($id);
      return view('Kredit/AR.form', compact('kredit'));
    }

     public function save(Request $request)
    {
         $input = $this->validate(request(), [
           'jenis_industri' => 'required',
           'keterlambatan' => 'required',
           'restrukturisasi' => 'required',
           'fasilitas_kredit' => 'required',
           'lama_kerjasama' => 'required',
           'badan_usaha' => 'required',
           'vendor_pemasok' => 'required',
           'afiliasi' => 'required',
           'kondisi_industri' => 'required',
           'opini_audit' => 'required',
           'audit_kap' => 'required',
           'posisi_tawar' => 'required'
        ]);

          $CustomerDetail=CustomerDetail::create([
            'credit_approval_id' => $request['ca_id'],
            'jenis_industri' => $request['jenis_industri'],
            'keterlambatan' => $request['keterlambatan'],
            'restrukturisasi' => $request['restrukturisasi'],
            'nilai_transaksi' => $request['nilai_transaksi'],
            'fasilitas_kredit' => $request['fasilitas_kredit'],
            'lama_kerjasama' => $request['lama_kerjasama'],
            'badan_usaha' => $request['badan_usaha'],
            'vendor_pemasok' => $request['vendor_pemasok'],
            'posisi_tawar' => $request['posisi_tawar'],
            'afiliasi' => $request['afiliasi'],
            'audit_kap' => $request['audit_kap'],
            'kondisi_industri' => $request['kondisi_industri'],
            'opini_audit' => $request['opini_audit'],
            'created_by' => Auth::user()->id
          ]);

          if ($CustomerDetail) {
            // $kredit= Kredit::find($CustomerDetail->credit_approval_id);
            // $kredit->status = '2';

            // $KreditController=new KreditController;
            // $KreditController->sendEmail($Kredit->customer_id, 4, $Kredit->id);

            // if ($kredit->save()) {
            $kredit=Kredit::find($request['ca_id']);
                return redirect('Kredit/'.$request['ca_id'].'')->with('success','Identitas Customer Berhasil Disimpan',compact('kredit'));
            // }
          }
    }

     public function edit($id)
    {
        $kredit = Kredit::find($id);
        $customer=Customer::get();
        $product=Product::get();
        return view('Kredit.AR.edit', compact('kredit','customer','product','id'));
    }

    public function cb_edit($id)
    {
        $kredit = Kredit::find($id);
        $customer=Customer::get();
        $product=Product::get();
        return view('Kredit.CashBank.edit', compact('kredit','customer','product','id'));
    }

    public function cb_update(Request $request, $id)
    {
      $kredit= Kredit::find($request['ca_id']);
      if ($request->hasFile('bank_garansi_doc')) {

          $input = $this->validate(request(), [
           'bank_garansi_doc' => 'required|mimes:pdf',
          ]);
          
          $bank_garansi_doc = $request->file('bank_garansi_doc');
          $bank_garansi_doc_extension = $bank_garansi_doc->getClientOriginalExtension();
          $bank_garansi_doc_fileName = str_random(5)."-".date('his')."-".str_random(3).".".$bank_garansi_doc_extension;
          $bank_garansi_doc_folderpath  = 'storage/upload/bank_garansi_doc'.'/';
          $bank_garansi_doc->move($bank_garansi_doc_folderpath , $bank_garansi_doc_fileName);

          $kredit->bank_garansi_doc = $bank_garansi_doc_folderpath.$bank_garansi_doc_fileName;
        }

        if ($request->hasFile('bank_konfirmasi_doc')) {

          $input = $this->validate(request(), [
           'bank_konfirmasi_doc' => 'required|mimes:pdf',
          ]);
          
          $bank_konfirmasi_doc = $request->file('bank_konfirmasi_doc');
          $bank_konfirmasi_doc_extension = $bank_konfirmasi_doc->getClientOriginalExtension();
          $bank_konfirmasi_doc_fileName = str_random(5)."-".date('his')."-".str_random(3).".".$bank_konfirmasi_doc_extension;
          $bank_konfirmasi_doc_folderpath  = 'storage/upload/bank_konfirmasi_doc'.'/';
          $bank_konfirmasi_doc->move($bank_konfirmasi_doc_folderpath , $bank_konfirmasi_doc_fileName);

          $kredit->bank_konfirmasi_doc = $bank_konfirmasi_doc_folderpath.$bank_konfirmasi_doc_fileName;
        }
          $kredit->save();
          // $kredit->status = '3';

        return redirect('Kredit/'.$request['ca_id'].'')->with('success','Dokumen Bank Berhasil Diubah');
    }

    public function fbs_edit($id)
    {
        $kredit = Kredit::find($id);
        $customer=Customer::get();
        $product=Product::get();
        return view('Kredit.Fbs.edit', compact('kredit','customer','product','id'));
    }

    public function fbs_update(Request $request, $id)
    {
      $nota = CaNota::where('credit_approval_id',$request['ca_id'])->first();
        $nota->tanggal_nota = $request->get('tanggal_nota');
        $nota->perihal = $request->get('perihal');
        $nota->isi = $request->get('isi');
        $nota->save();

        $kredit= Kredit::find($request['ca_id']);
        if ($request->hasFile('credit_scoring')) {

          $input = $this->validate(request(), [
           'credit_scoring' => 'required|mimes:pdf',
          ]);
          
         $credit_scoring = $request->file('credit_scoring');
          $credit_scoring_extension = $credit_scoring->getClientOriginalExtension();
          $credit_scoring_fileName = str_random(5)."-".date('his')."-".str_random(3).".".$credit_scoring_extension;
          $credit_scoring_folderpath  = 'storage/upload/credit_scoring'.'/';
          $credit_scoring->move($credit_scoring_folderpath , $credit_scoring_fileName);

          $kredit->credit_scoring_doc = $credit_scoring_folderpath.$credit_scoring_fileName;
        }
          $kredit->save();
          // $kredit->status = '3';

        return redirect('Kredit/'.$request['ca_id'].'')->with('success','Nota dan Credit Scoring Berhasil Diubah');
    }

     public function update(Request $request, $id)
    {
        $input = $this->validate(request(), [
           'jenis_industri' => 'required',
           'keterlambatan' => 'required',
           'restrukturisasi' => 'required',
           'fasilitas_kredit' => 'required',
           'lama_kerjasama' => 'required',
           'badan_usaha' => 'required',
           'vendor_pemasok' => 'required',
           'afiliasi' => 'required',
           'kondisi_industri' => 'required',
           'opini_audit' => 'required',
           'audit_kap' => 'required',
           'posisi_tawar' => 'required'
        ]);

        $CDCA = CustomerDetail::find($id);
        $CDCA->jenis_industri = $request->get('jenis_industri');
        $CDCA->keterlambatan = $request->get('keterlambatan');
        $CDCA->restrukturisasi = $request->get('restrukturisasi');
        $CDCA->fasilitas_kredit = $request->get('fasilitas_kredit');
        $CDCA->lama_kerjasama = $request->get('lama_kerjasama');
        $CDCA->badan_usaha = $request->get('badan_usaha');
        $CDCA->vendor_pemasok = $request->get('vendor_pemasok');
        $CDCA->afiliasi = $request->get('afiliasi');
        $CDCA->kondisi_industri = $request->get('kondisi_industri');
        $CDCA->opini_audit = $request->get('opini_audit');
        $CDCA->audit_kap = $request->get('audit_kap');
        $CDCA->posisi_tawar = $request->get('posisi_tawar');
        $CDCA->save();

        return redirect('Kredit/'.$request['ca_id'].'')->with('success','Identitas Customer Berhasil Diubah');
    }

    public function submit(Request $request, $id, $status)
    {
        $kredit = Kredit::find($id);
        $kredit->status = $status;
        $kredit->save();
        $KreditController=new KreditController;
        $KreditController->sendEmail($Kredit->customer_id, $role_id, $Kredit->id);
       return redirect('Kredit.show')->with('success',''.$kredit->Customer->name.' telah di Submit');
    }

// Cash Bank
    public function upload($id) 
    {
      $kredit = Kredit::find($id);
      return view('Kredit/CashBank.form', compact('kredit'));
    }

     public function upload_save(Request $request)
    {
         $input = $this->validate(request(), [
           'bank_garansi_doc' => 'required|mimes:pdf',
           'bank_konfirmasi_doc' => 'required|mimes:pdf'
        ]);

          $bank_garansi_doc = $request->file('bank_garansi_doc');
          $bank_garansi_doc_extension = $bank_garansi_doc->getClientOriginalExtension();
          $bank_garansi_doc_fileName = str_random(5)."-".date('his')."-".str_random(3).".".$bank_garansi_doc_extension;
          $bank_garansi_doc_folderpath  = 'storage/upload/bank_garansi_doc'.'/';
          $bank_garansi_doc->move($bank_garansi_doc_folderpath , $bank_garansi_doc_fileName);

          $bank_konfirmasi_doc = $request->file('bank_konfirmasi_doc');
          $bank_konfirmasi_doc_extension = $bank_konfirmasi_doc->getClientOriginalExtension();
          $bank_konfirmasi_doc_fileName = str_random(5)."-".date('his')."-".str_random(3).".".$bank_konfirmasi_doc_extension;
          $bank_konfirmasi_doc_folderpath  = 'storage/upload/bank_konfirmasi_doc'.'/';
          $bank_konfirmasi_doc->move($bank_konfirmasi_doc_folderpath , $bank_konfirmasi_doc_fileName);

          $kredit= Kredit::find($request['ca_id']);
          $kredit->bank_garansi_doc = $bank_garansi_doc_folderpath.$bank_garansi_doc_fileName;
          $kredit->bank_konfirmasi_doc = $bank_konfirmasi_doc_folderpath.$bank_konfirmasi_doc_fileName;
          // $kredit->status = '3';
          $kredit->save();

          $KreditController=new KreditController;
          $KreditController->sendEmail($Kredit->customer_id, 5, $Kredit->id);

          return redirect('Kredit/'.$request['ca_id'].'')->with('success','Dokumen Bank Berhasil Disimpan');
        }

// Finance Business Support
    public function form_cs($id) 
    {
      $kredit = Kredit::find($id);
      return view('Kredit/Fbs.form', compact('kredit'));
    }

     public function cs_save(Request $request)
    {
         $input = $this->validate(request(), [
           'credit_scoring' => 'required|mimes:pdf',
        ]);

          $credit_scoring = $request->file('credit_scoring');
          $credit_scoring_extension = $credit_scoring->getClientOriginalExtension();
          $credit_scoring_fileName = str_random(5)."-".date('his')."-".str_random(3).".".$credit_scoring_extension;
          $credit_scoring_folderpath  = 'storage/upload/credit_scoring'.'/';
          $credit_scoring->move($credit_scoring_folderpath , $credit_scoring_fileName);

          $kredit= Kredit::find($request['ca_id']);
          $kredit->credit_scoring_doc = $credit_scoring_folderpath.$credit_scoring_fileName;
          // $kredit->status = '4';
          $kredit->save();

          $KreditController=new KreditController;
          $KreditController->sendEmail($Kredit->customer_id, 6, $Kredit->id);

          return redirect('Kredit/'.$request['ca_id'].'')->with('success','Credit Scoring Berhasil Ditambahkan');
        }


    // Komite Kredit
    public function kk_upload($id) 
    {
      $kredit = Kredit::find($id);
      return view('Kredit/KK.form', compact('kredit'));
    }

     public function kk_upload_save(Request $request)
    {
         $input = $this->validate(request(), [
           'coval_doc' => 'required|mimes:pdf',
        ]);

          $coval_doc = $request->file('coval_doc');
          $coval_doc_extension = $coval_doc->getClientOriginalExtension();
          $coval_doc_fileName = str_random(5)."-".date('his')."-".str_random(3).".".$coval_doc_extension;
          $coval_doc_folderpath  = 'storage/upload/coval_doc'.'/';
          $coval_doc->move($coval_doc_folderpath , $coval_doc_fileName);

          $kredit= Kredit::find($request['ca_id']);
          $kredit->credit_approval_doc = $coval_doc_folderpath.$coval_doc_fileName;
          // $kredit->status = '4';
          $kredit->save();

          $KreditController=new KreditController;
          $KreditController->sendEmail($Kredit->customer_id, 6, $Kredit->id);

          return redirect('Kredit/'.$request['ca_id'].'')->with('success','Credit Scoring Berhasil Ditambahkan');
        }

        public function kk_edit($id)
        {
            $kredit = Kredit::find($id);
            $customer=Customer::get();
            $product=Product::get();
            return view('Kredit.KK.edit', compact('kredit','customer','product','id'));
        }

        public function kk_update(Request $request, $id)
        {
          $kredit= Kredit::find($request['ca_id']);
          if ($request->hasFile('coval_doc')) {
             
             $input = $this->validate(request(), [
                 'coval_doc' => 'required|mimes:pdf',
              ]);

              $coval_doc = $request->file('coval_doc');
              $coval_doc_extension = $coval_doc->getClientOriginalExtension();
              $coval_doc_fileName = str_random(5)."-".date('his')."-".str_random(3).".".$coval_doc_extension;
              $coval_doc_folderpath  = 'storage/upload/coval_doc'.'/';
              $coval_doc->move($coval_doc_folderpath , $coval_doc_fileName);

              $kredit->coval_doc = $coval_doc_folderpath.$coval_doc_fileName;
            }

            $kredit->save();
            // $kredit->status = '3';

            return redirect('Kredit/'.$request['ca_id'].'')->with('success','Dokumen Bank Berhasil Diubah');
        }



    public function showPdfIdentitasCustomer(Request $request, $id) {
      $data = CustomerDetail::find($id);
      view()->share('data',$data);
      $pdf = PDF::loadView('Pdf/IdentitasCustomer');
      return $pdf->download('Pdf/IdentitasCustomer.pdf')
                 ->header('Content-Type', 'application/pdf');
    }

     public function index()
    { 
      if (Auth::user()->isRole('user')) {
          $kredit=Kredit::where('status', '>=', 0)->get();
      }elseif (Auth::user()->isRole('ar')) {
          $kredit=Kredit::where('status', '>=', 1)->get();
      }elseif (Auth::user()->isRole('cashbank')) {
          $kredit=Kredit::where('status', '>=', 2)->get();
      }elseif (Auth::user()->isRole('fbs')) {
          $kredit=Kredit::where('status', '>=', 3)->get();
      }elseif (Auth::user()->isRole('manajemenresiko')) {
          $kredit=Kredit::where('status', '>=', 4)->get();
      }elseif (Auth::user()->isRole('komitekredit')) {
          $kredit=Kredit::where('status', '>=', 5)->get();
      }elseif (Auth::user()->isRole('administrator')) {
          $kredit=Kredit::get();
      }
        return view('Kredit.index', compact('kredit'));
        
    }

}
