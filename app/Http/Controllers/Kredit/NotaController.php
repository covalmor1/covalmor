<?php

namespace App\Http\Controllers\Kredit;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Kredit\Kredit;
use App\Models\Transaction\CaNota;
use PDF;

class NotaController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }
// Account Receivable (AR)
    public function entry($id) 
    {
      $kredit = Kredit::find($id);
      return view('Kredit/Fbs.entry', compact('kredit'));
    }

     public function nota_save(Request $request)
    {
         $input = $this->validate(request(), [
           'tanggal_nota' => 'required',
           'perihal' => 'required',
           'isi' => 'required'
        ]);



          $Nota=CaNota::create([
            'credit_approval_id' => $request['ca_id'],
            'tanggal_nota' => $request['tanggal_nota'],
            'perihal' => $request['perihal'],
            'isi' => $request['isi'],
            'created_by' => Auth::user()->id
          ]);

        
        return redirect('Kredit/'.$request['ca_id'].'')->with('success','Nota Pengantar berhasil disimpan');
    }

     public function index()
    { 
      if (Auth::user()->isRole('user')) {
          $kredit=Kredit::where('status', '>=', 0)->get();
      }elseif (Auth::user()->isRole('ar')) {
          $kredit=Kredit::where('status', '>=', 1)->get();
      }elseif (Auth::user()->isRole('cashbank')) {
          $kredit=Kredit::where('status', '>=', 2)->get();
      }elseif (Auth::user()->isRole('fbs')) {
          $kredit=Kredit::where('status', '>=', 3)->get();
      }elseif (Auth::user()->isRole('manajemenresiko')) {
          $kredit=Kredit::where('status', '>=', 4)->get();
      }elseif (Auth::user()->isRole('komitekredit')) {
          $kredit=Kredit::where('status', '>=', 5)->get();
      }elseif (Auth::user()->isRole('administrator')) {
          $kredit=Kredit::get();
      }
        return view('Kredit.index', compact('kredit'));
        
    }

}
