<?php

namespace App\Http\Controllers\Master;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Master\Product as Product;
use Session;
use Excel;
use File;

class ProductController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
     public function create()
    {
        return view('Master/Product.create');
    }

     public function store(Request $request)
    {
        $input = $this->validate(request(), [
           'product_no' => 'required',
           'name' => 'required'
        ]);

        $Product=Product::create([
          'material_no' => $request['product_no'],
          'material_name' => $request['name'],
          'material_group' => $request['group'],
        ]);

        return redirect('Master/Product')->with('success','Product telah ditambahkan');
    }

     public function index()
    {
        $product = Product::get();
        return view('Master/Product.index', compact('product'));
        
    }

    public function filter(Request $request)
    {
         return view('Master/Product.index');
    }

     public function edit($id)
    {
        $product = Product::find($id);
        return view('Master/Product.edit', compact('product'));
    }

     public function update(Request $request, $id)
    {
        $product = Product::find($id);
        $product->material_no = $request->get('product_no');
        $product->material_name = $request->get('name');
        $product->material_group = $request->get('group');
        $product->save();

       return redirect('Master/Product')->with('success','Product telah di ubah');
    }
  
    public function show($id)
    {
        // $product = product::find($id);
        //  return view('Master/product.show', compact('product'));
    }

     public function destroy($id)
    {
        $product = Product::find($id);
        $product->delete();
        return redirect('Master/Product')->with('success','Product telah di hapus');
    }

     public function export()
    {
        $product = Product::get();
        return $product;


    }

    

public function UploadUpdate(Request $request){

        //validate the xls file
        $this->validate($request, array(
            'file'      => 'required'
        ));
 
        if($request->hasFile('file')){
            $extension = File::extension($request->file->getClientOriginalName());
            if ($extension == "xlsx" || $extension == "xls" || $extension == "csv") {
 
                $path = $request->file->getRealPath();
                $data = Excel::load($path, function($reader) {
                })->get();
                if(!empty($data) && $data->count()){
 
                    foreach ($data as $key => $value) {
                        $insert[] = [
                        'material_no' => $value->material_no,
                        'material_name' => $value->material_name,
                        'material_group' => $value->material_group,
                        ];
                    }

                    if(!empty($insert)){
                        $insertData = Product::insert($insert);
                        if ($insertData) {
                            Session::flash('success', 'Your Data has successfully imported');
                        }else {                        
                            Session::flash('error', 'Error inserting the data..');
                            return back();
                        }
                    }
                }
 
                return back();
 
            }else {
                Session::flash('error', 'File is a '.$extension.' file.!! Please upload a valid xls/csv file..!!');
                return back();
            }
        }
}

}
