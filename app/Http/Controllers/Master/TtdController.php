<?php

namespace App\Http\Controllers\Master;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Master\Ttd;
use Session;
use Excel;
use File;

class TtdController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
     public function create()
    {
        return view('Master/Ttd.create');
    }

     public function store(Request $request)
    {
        $input = $this->validate(request(), [
           'name1' => 'required',
           'name2' => 'required',
           'position1' => 'required',
           'position2' => 'required',
           'document_type' => 'required'
        ]);

        $Ttd=Ttd::create([
          'name1' => $request['name1'],
          'name2' => $request['name2'],
          'position1' => $request['position1'],
          'position2' => $request['position2'],
          'document_type' => $request['document_type']
        ]);

        return redirect('Master/Ttd')->with('success','Ttd telah ditambahkan');
    }

     public function index()
    {
        $ttd = Ttd::get();
        return view('Master/Ttd.index', compact('ttd'));
        
    }

    public function filter(Request $request)
    {
         return view('Master/Ttd.index');
    }

     public function edit($id)
    {
        $ttd = Ttd::find($id);
        return view('Master/Ttd.edit', compact('ttd'));
    }

     public function update(Request $request, $id)
    {
        $ttd = Ttd::find($id);
        $ttd->name1 = $request->get('name1');
        $ttd->name2 = $request->get('name2');
        $ttd->position1 = $request->get('position1');
        $ttd->position2 = $request->get('position2');
        $ttd->document_type = $request->get('document_type');

        $ttd->save();

       return redirect('Master/Ttd')->with('success','Ttd telah di ubah');
    }
  
    public function show($id)
    {
    }

     public function destroy($id)
    {
        $ttd = Ttd::find($id);
        $ttd->delete();
        return redirect('Master/Ttd')->with('success','Ttd telah di hapus');
    }

     public function export()
    {
        $ttd = Ttd::get();
        return $ttd;


    }

    

public function UploadUpdate(Request $request){

        //validate the xls file
        $this->validate($request, array(
            'file'      => 'required'
        ));
 
        if($request->hasFile('file')){
            $extension = File::extension($request->file->getClientOriginalName());
            if ($extension == "xlsx" || $extension == "xls" || $extension == "csv") {
 
                $path = $request->file->getRealPath();
                $data = Excel::load($path, function($reader) {
                })->get();
                if(!empty($data) && $data->count()){
 
                    foreach ($data as $key => $value) {
                        $insert[] = [
                        'name1' => $value->name1,
                        'name2' => $value->name2,
                        'position1' => $value->position1,
                        'position2' => $value->position2,
                        'document_type' => $value->document_type,
                        ];
                    }

                    if(!empty($insert)){
                        $insertData = Ttd::insert($insert);
                        if ($insertData) {
                            Session::flash('success', 'Your Data has successfully imported');
                        }else {                        
                            Session::flash('error', 'Error inserting the data..');
                            return back();
                        }
                    }
                }
 
                return back();
 
            }else {
                Session::flash('error', 'File is a '.$extension.' file.!! Please upload a valid xls/csv file..!!');
                return back();
            }
        }
}

}
