<?php

namespace App\Models\Transaction;

use Illuminate\Database\Eloquent\Model;

class CaActionNote extends Model
{
    protected $table = "tr_ca_action_note";
    // public $timestamps = false;
    protected $fillable = [
        'credit_approval_id',
        'action_note',
        'action_type',
        'action_time',
        'action_by'
    ];
    
    public function CreditApproval() {
        return $this->belongsTo('App\Models\Kredit\Kredit');
    }

    public function Creator() {
        return $this->belongsTo('App\User','action_by');
    }

    public function CreatorRole() {
        return $this->belongsTo('App\Models\Auth\Role_user','action_by', 'user_id');
    }
}
