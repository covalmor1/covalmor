<?php

namespace App\Models\Transaction;

use Illuminate\Database\Eloquent\Model;

class NumGenerator extends Model
{
    protected $table = "num_generator";
    // public $timestamps = false;
    protected $fillable = [
        'number'
    ];
}
