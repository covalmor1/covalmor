<?php

namespace App\Models\Transaction;

use Illuminate\Database\Eloquent\Model;

class CaNota extends Model
{
    protected $table = "tr_ca_nota";
    // public $timestamps = false;
    protected $fillable = [
        'credit_approval_id',
        'tanggal_nota',
        'perihal',
        'isi',
        'created_by'
    ];

    public function CreditApproval() {
        return $this->belongsTo('App\Models\Kredit\Kredit');
    }

    public function Creator() {
        return $this->belongsTo('App\User','created_by');
    }
}
