<?php

namespace App\Models\Transaction;

use Illuminate\Database\Eloquent\Model;

class CaProduct extends Model
{
    protected $table = "tr_ca_product";
    // public $timestamps = false;
    protected $fillable = [
        'credit_approval_id',
        'product_id'
    ];

    public function CreditApproval() {
        return $this->belongsTo('App\Models\Kredit\Kredit');
    }

    public function Detail() {
        return $this->hasOne('App\Models\Master\Product','id','product_id');
    }
}
