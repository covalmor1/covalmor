<?php

namespace App\Models\Kredit;

use Illuminate\Database\Eloquent\Model;

class CustomerDetail extends Model
{
    protected $table = "ca_customer_detail";
    // public $timestamps = false;
    protected $fillable = [
        'credit_approval_id',
    	'jenis_industri',
    	'keterlambatan',
    	'restrukturisasi',
    	'fasilitas_kredit',
    	'lama_kerjasama',
        'vendor_pemasok',
        'posisi_tawar',
        'badan_usaha',
        'afiliasi',
        'kondisi_industri',
        'opini_audit',
        'syarat_penyerahan',
        'audit_kap',
        'created_by'
    ];

    public function Creator() {
        return $this->belongsTo('App\User','created_by');
    }

    public function Kredit() {
        return $this->belongsTo('App\Models\Kredit\Kredit');
    }

}