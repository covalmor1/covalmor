<?php

namespace App\Models\Kredit;

use Illuminate\Database\Eloquent\Model;

class Kredit extends Model
{
    protected $table = "credit_approval";
    // public $timestamps = false;
    protected $fillable = [
        'no_tiket',
        'no_surat',
    	'customer_id',
    	'tempo_start',
    	'tempo_end',
        'volume',
        'satuan',
    	'periode_volume',
    	'nilai_transaksi',
        'credit_limit',
        'pembayaran',
        'jaminan',
        'periode_penyerahan',
        'memo_pengantar',
        'doc_lka',
        'doc_cas',
        'doc_bg',
        'doc_pml',
        'bank_garansi_doc',
        'bank_konfirmasi_doc',
        'flag_denda',
        'lama_tempo',
        'syarat_penyerahan',
        'status',
        'flag_read',
        'created_by',
        'submit_ar_date',
        'submit_cb_date',
        'submit_fbs_date',
        'submit_mr_date',
        'submit_kk_date',
        'note',
        'credit_approval_doc',
    ];

    public function Creator() {
        return $this->belongsTo('App\User','created_by');
    }

    public function Customer() {
        return $this->belongsTo('App\Models\Master\Customer','customer_id','id');
    }

    public function CustomerDetail() {
        return $this->hasOne('App\Models\Kredit\CustomerDetail','credit_approval_id');
    }

    public function Nota() {
        return $this->hasOne('App\Models\Transaction\CaNota','credit_approval_id');
    }

    public function Product() {
        return $this->hasMany('App\Models\Transaction\CaProduct','credit_approval_id');
    }

    public function Note() {
        return $this->hasMany('App\Models\Transaction\CaActionNote','credit_approval_id');
    }

    public function Status() {
        return $this->belongsTo('App\Models\Master\Status','status','id');
    }

}