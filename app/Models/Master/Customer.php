<?php

namespace App\Models\Master;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    protected $table = "m_customer";
    // public $timestamps = false;
    protected $fillable = [
        'customer_no',
        'name',
        'email',
        'address',
        'npwp'
    ];

    
}
