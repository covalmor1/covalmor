<?php

namespace App\Models\Master;

use Illuminate\Database\Eloquent\Model;

class Status extends Model
{
    protected $table = "m_status";
    // public $timestamps = false;
    protected $fillable = [
        'name',
        'description',
        'class'
    ];
}
