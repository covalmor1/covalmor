<?php

namespace App\Models\Master;

use Illuminate\Database\Eloquent\Model;

class Ttd extends Model
{
    protected $table = "ttd";
    // public $timestamps = false;
    protected $fillable = [
        'name1',
        'name2',
        'position1',
        'position2',
        'document_type'
    ];

    
}
