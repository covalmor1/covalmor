<?php

namespace App\Models\Master;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table = "ca_product";
    // public $timestamps = false;
    protected $fillable = [
        'material_no',
        'material_name',
        'material_group'
    ];

    public function CaProduct() {
        return $this->hasMany('App\Models\Transaction\CaProduct','id','product_id');
    }
}
