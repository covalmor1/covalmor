<?php

use Illuminate\Database\Seeder;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Seed New Role
        DB::table('roles')->insert([
            'id'          => 1,
            'name'        => 'Administrator',
            'slug'        => 'administrator',
            'description' => 'Administrator User',
            'special'     => 'all-access'
        ]);

        DB::table('roles')->insert([
            'id'          => 2,
            'name'        => 'User',
            'slug'        => 'user',
            'description' => 'User'
        ]);

        DB::table('roles')->insert([
            'id'          => 3,
            'name'        => 'Account Receivable',
            'slug'        => 'ar',
            'description' => 'Account Receivable (AR)'
        ]);

        DB::table('roles')->insert([
            'id'          => 4,
            'name'        => 'Cash and Bank',
            'slug'        => 'cashbank',
            'description' => 'Cash & Bank'
        ]);

        DB::table('roles')->insert([
            'id'          => 5,
            'name'        => 'Finance Business Support',
            'slug'        => 'fbs',
            'description' => 'Finance Business Support'
        ]);

        DB::table('roles')->insert([
            'id'          => 6,
            'name'        => 'Manajemen Resiko',
            'slug'        => 'manajemenresiko',
            'description' => 'Manajemen Resiko (Manajer Keuangan)'
        ]);

          DB::table('roles')->insert([
            'id'          => 7,
            'name'        => 'Komite Kredit',
            'slug'        => 'komitekredit',
            'description' => 'Komite Kredit (General Manager)'
        ]);
    }
}
