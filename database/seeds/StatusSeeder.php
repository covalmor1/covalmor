<?php

use Illuminate\Database\Seeder;

class StatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Seed New Status
        DB::table('m_status')->insert([
            'id'          => 0,
            'name'        => 'Draft User',
            'class'     => 'warning'
        ]);

        DB::table('m_status')->insert([
            'id'          => 1,
            'name'        => 'AR',
            'class'     => 'warning'
        ]);

        DB::table('m_status')->insert([
            'id'          => 2,
            'name'        => 'Cash Bank',
            'class'     => 'warning'
        ]);

        DB::table('m_status')->insert([
            'id'          => 3,
            'name'        => 'FBS',
            'class'     => 'success'
        ]);

        DB::table('m_status')->insert([
            'id'          => 4,
            'name'        => 'Manajemen Resiko',
            'class'     => 'success'
        ]);

        DB::table('m_status')->insert([
            'id'          => 5,
            'name'        => 'Komite Kredit',
            'class'     => 'primary'
        ]);

        DB::table('m_status')->insert([
            'id'          => 6,
            'name'        => 'Completed',
            'class'     => 'primary'
        ]);

    }
}
