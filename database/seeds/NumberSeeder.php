<?php

use Illuminate\Database\Seeder;

class NumberSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Seed New Status
        DB::table('num_generator')->insert([
            'id'          => 1,
            'number'        => '00',
            'created_at'     => date("Y-m-d h:i:s"),
            'updated_at'     => date("Y-m-d h:i:s")
        ]);

    }
}
