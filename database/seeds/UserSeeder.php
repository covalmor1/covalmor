<?php

use Illuminate\Database\Seeder;
use App\User;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::firstOrCreate(
            [
                'email'     => 'admin@covalmor1.com',
                'password'  => bcrypt('12345678'),
                'name'      => 'Administrator'
            ]
        )->assignRole(1);

        User::firstOrCreate(
            [
                'email'     => 'user@covalmor1.com',
                'password'  => bcrypt('12345678'),
                'name'      => 'User'
            ]
        )->assignRole(2);

        User::firstOrCreate(
            [
                'email'     => 'ar@covalmor1.com',
                'password'  => bcrypt('12345678'),
                'name'      => 'Account Receivable'
            ]
        )->assignRole(3);

        User::firstOrCreate(
            [
                'email'     => 'cashbank@covalmor1.com',
                'password'  => bcrypt('12345678'),
                'name'      => 'Cash Bank'
            ]
        )->assignRole(4);

        User::firstOrCreate(
            [
                'email'     => 'fbs@covalmor1.com',
                'password'  => bcrypt('12345678'),
                'name'      => 'Finance Business Support'
            ]
        )->assignRole(5);

        User::firstOrCreate(
            [
                'email'     => 'manajemenresiko@covalmor1.com',
                'password'  => bcrypt('12345678'),
                'name'      => 'Manajemen Resiko'
            ]
        )->assignRole(6);

        User::firstOrCreate(
            [
                'email'     => 'komitekredit@covalmor1.com',
                'password'  => bcrypt('12345678'),
                'name'      => 'Komite Kredit'
            ]
        )->assignRole(7);
    }
}
